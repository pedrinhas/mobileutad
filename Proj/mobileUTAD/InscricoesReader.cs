﻿// Decompiled with JetBrains decompiler
// Type: mobileUTAD.InscricoesReader
// Assembly: mobileUTAD, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C6335312-308C-477B-9BDC-E68404688049
// Assembly location: A:\Git repositories\mobileUTAD\bin\mobileUTAD.dll

using mobileUTAD.SIGACADClient;
using System.Collections.Generic;

namespace mobileUTAD
{
  public class InscricoesReader
  {
    public static PlanoCurricular Read(string numAluno, mobileUTAD.SIGACADClient.SIGACADClient sigacad)
    {
      PlanoCurricular planoCurricular = new PlanoCurricular();
      List<Disciplina> disciplinaList = new List<Disciplina>();
      Curso curso = new Curso();
      List<Curso> cursoList = new List<Curso>();
      List<alunoInscricoes> alunoInscricoesList = new List<alunoInscricoes>();
      if (alunoInscricoesList != null)
      {
        alunoInscricoesList.AddRange((IEnumerable<alunoInscricoes>) sigacad.GetAlunoInscricoes(numAluno));
        string str = "";
        foreach (alunoInscricoes alunoInscricoes in alunoInscricoesList)
        {
          if (alunoInscricoes.curso.ToString() != str)
          {
            str = alunoInscricoes.curso;
            curso.Nome = str;
            if (disciplinaList.Count > 0)
            {
              curso.Disciplinas = disciplinaList;
              cursoList.Add(curso);
              curso = new Curso();
              disciplinaList = new List<Disciplina>();
            }
          }
          disciplinaList.Add(new Disciplina()
          {
            Nome = alunoInscricoes.nomedisciplina + (alunoInscricoes.nomedisciplina.Contains("OPÇÃO") ? " (?)" : ""),
            Posicao = string.Format("[A{0}/S{1} - {2} ECTS]", (object) alunoInscricoes.anoCurricular, (object) alunoInscricoes.semestre, (object) alunoInscricoes.ects)
          });
        }
        curso.Disciplinas = disciplinaList;
        cursoList.Add(curso);
        List<AlunoSIGACAD> alunoSigacadList = new List<AlunoSIGACAD>();
        if (alunoSigacadList != null)
        {
          try
          {
            AlunoSIGACAD[] alunoDadosPessoais = sigacad.GetAlunoDadosPessoais(numAluno);
            if (alunoDadosPessoais != null)
            {
              alunoSigacadList.AddRange((IEnumerable<AlunoSIGACAD>) alunoDadosPessoais);
              foreach (AlunoSIGACAD alunoSigacad in alunoSigacadList)
              {
                planoCurricular.Numero = alunoSigacad.NumProcesso;
                planoCurricular.Nome = alunoSigacad.Nome;
                planoCurricular.Regime = alunoSigacad.nomeRegime;
              }
            }
          }
          catch
          {
          }
        }
        planoCurricular.Curso = cursoList;
      }
      return planoCurricular;
    }
  }
}
