﻿// Decompiled with JetBrains decompiler
// Type: mobileUTAD.DividasReader
// Assembly: mobileUTAD, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C6335312-308C-477B-9BDC-E68404688049
// Assembly location: A:\Git repositories\mobileUTAD\bin\mobileUTAD.dll

using mobileUTAD.SIGACADClient;
using System.Collections.Generic;

namespace mobileUTAD
{
  public class DividasReader
  {
    public static PlanoDividas Read(string numAluno, mobileUTAD.SIGACADClient.SIGACADClient sigacad)
    {
      PlanoDividas planoDividas = new PlanoDividas();
      List<Divida> dividaList = new List<Divida>();
      List<dividasAluno> dividasAlunoList = new List<dividasAluno>();
      if (dividasAlunoList != null)
      {
        dividasAlunoList.AddRange((IEnumerable<dividasAluno>) sigacad.GetAlunoDividas(numAluno));
        foreach (dividasAluno dividasAluno in dividasAlunoList)
          dividaList.Add(new Divida()
          {
            Data = dividasAluno.datadivida,
            Descricao = dividasAluno.descritivo,
            Tipo = dividasAluno.nometipodivida,
            Valor = dividasAluno.valordivida.Replace("*", " €")
          });
        planoDividas.Divida = dividaList;
        List<AlunoSIGACAD> alunoSigacadList = new List<AlunoSIGACAD>();
        if (alunoSigacadList != null)
        {
          try
          {
            AlunoSIGACAD[] alunoDadosPessoais = sigacad.GetAlunoDadosPessoais(numAluno);
            if (alunoDadosPessoais != null)
            {
              alunoSigacadList.AddRange((IEnumerable<AlunoSIGACAD>) alunoDadosPessoais);
              foreach (AlunoSIGACAD alunoSigacad in alunoSigacadList)
              {
                planoDividas.Numero = alunoSigacad.NumProcesso;
                planoDividas.Nome = alunoSigacad.Nome;
                planoDividas.Regime = alunoSigacad.nomeRegime;
              }
            }
          }
          catch
          {
          }
        }
      }
      return planoDividas;
    }
  }
}
