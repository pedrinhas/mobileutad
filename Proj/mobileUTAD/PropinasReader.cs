﻿// Decompiled with JetBrains decompiler
// Type: mobileUTAD.PropinasReader
// Assembly: mobileUTAD, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C6335312-308C-477B-9BDC-E68404688049
// Assembly location: A:\Git repositories\mobileUTAD\bin\mobileUTAD.dll

using mobileUTAD.SIGACADClient;
using System.Collections.Generic;

namespace mobileUTAD
{
  public class PropinasReader
  {
    public static Propinas Read(string numAluno, mobileUTAD.SIGACADClient.SIGACADClient sigacad)
    {
      Propinas propinas = new Propinas();
      List<Referencia> referenciaList = new List<Referencia>();
      CursoReferencia cursoReferencia = new CursoReferencia();
      List<CursoReferencia> cursoReferenciaList = new List<CursoReferencia>();
      List<propinasAluno> propinasAlunoList = new List<propinasAluno>();
      if (propinasAlunoList != null)
      {
        propinasAlunoList.AddRange((IEnumerable<propinasAluno>) sigacad.GetAlunoPropinasPagas(numAluno));
        string str = "";
        foreach (propinasAluno propinasAluno in propinasAlunoList)
        {
          if (propinasAluno.curso.ToString() != str)
          {
            str = propinasAluno.curso;
            cursoReferencia.Nome = str;
            if (referenciaList.Count > 0)
            {
              cursoReferencia.Referencias = referenciaList;
              cursoReferenciaList.Add(cursoReferencia);
              cursoReferencia = new CursoReferencia();
              referenciaList = new List<Referencia>();
            }
          }
          referenciaList.Add(new Referencia()
          {
            Ano = propinasAluno.anoLectivo,
            DataLimite = propinasAluno.datalimitepag,
            Prestacao = propinasAluno.prestacao,
            Valor = propinasAluno.valor
          });
        }
        cursoReferencia.Referencias = referenciaList;
        cursoReferenciaList.Add(cursoReferencia);
        List<AlunoSIGACAD> alunoSigacadList = new List<AlunoSIGACAD>();
        if (alunoSigacadList != null)
        {
          try
          {
            AlunoSIGACAD[] alunoDadosPessoais = sigacad.GetAlunoDadosPessoais(numAluno);
            if (alunoDadosPessoais != null)
            {
              alunoSigacadList.AddRange((IEnumerable<AlunoSIGACAD>) alunoDadosPessoais);
              foreach (AlunoSIGACAD alunoSigacad in alunoSigacadList)
              {
                propinas.Numero = alunoSigacad.NumProcesso;
                propinas.Nome = alunoSigacad.Nome;
                propinas.Regime = alunoSigacad.nomeRegime;
              }
            }
          }
          catch
          {
          }
        }
        propinas.Curso = cursoReferenciaList;
      }
      return propinas;
    }
  }
}
