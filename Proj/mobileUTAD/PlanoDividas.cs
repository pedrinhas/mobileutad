﻿// Decompiled with JetBrains decompiler
// Type: mobileUTAD.PlanoDividas
// Assembly: mobileUTAD, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C6335312-308C-477B-9BDC-E68404688049
// Assembly location: A:\Git repositories\mobileUTAD\bin\mobileUTAD.dll

using System.Collections.Generic;

namespace mobileUTAD
{
  public class PlanoDividas
  {
    public string Numero { get; set; }

    public string Nome { get; set; }

    public string Regime { get; set; }

    public List<mobileUTAD.Divida> Divida { get; set; }
  }
}
