﻿// Decompiled with JetBrains decompiler
// Type: mobileUTAD.DadosPessoaisReader
// Assembly: mobileUTAD, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C6335312-308C-477B-9BDC-E68404688049
// Assembly location: A:\Git repositories\mobileUTAD\bin\mobileUTAD.dll

using mobileUTAD.SIGACADClient;
using System.Collections.Generic;

namespace mobileUTAD
{
  public class DadosPessoaisReader
  {
    public static Aluno Read(string numAluno, mobileUTAD.SIGACADClient.SIGACADClient sigacad)
    {
      Aluno aluno = new Aluno();
      List<Inscricao> inscricaoList = new List<Inscricao>();
      Inscricao inscricao = new Inscricao();
      List<alunoCursos> alunoCursosList = new List<alunoCursos>();
      if (alunoCursosList != null)
      {
        alunoCursosList.AddRange((IEnumerable<alunoCursos>) sigacad.GetAlunoCursos(numAluno));
        foreach (alunoCursos alunoCursos in alunoCursosList)
        {
          inscricao.Numero = alunoCursos.codCurso;
          inscricao.Nome = alunoCursos.nomeCurso;
          inscricaoList.Add(inscricao);
        }
        List<AlunoSIGACAD> alunoSigacadList = new List<AlunoSIGACAD>();
        if (alunoSigacadList != null)
        {
          try
          {
            AlunoSIGACAD[] alunoDadosPessoais = sigacad.GetAlunoDadosPessoais(numAluno);
            if (alunoDadosPessoais != null)
            {
              alunoSigacadList.AddRange((IEnumerable<AlunoSIGACAD>) alunoDadosPessoais);
              foreach (AlunoSIGACAD alunoSigacad in alunoSigacadList)
              {
                aluno.Numero = alunoSigacad.NumProcesso;
                aluno.Nome = alunoSigacad.Nome;
                aluno.Regime = alunoSigacad.nomeRegime;
              }
            }
          }
          catch
          {
          }
        }
        aluno.Inscricoes = inscricaoList;
      }
      return aluno;
    }
  }
}
