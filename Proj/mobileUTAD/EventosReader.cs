﻿// Decompiled with JetBrains decompiler
// Type: mobileUTAD.EventosReader
// Assembly: mobileUTAD, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C6335312-308C-477B-9BDC-E68404688049
// Assembly location: A:\Git repositories\mobileUTAD\bin\mobileUTAD.dll

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace mobileUTAD
{
  public class EventosReader
  {
    public static List<Eventos> Read(string url)
    {
      WebResponse webResponse = (WebResponse) null;
      try
      {
        WebRequest webRequest = WebRequest.Create(url);
        webRequest.Timeout = 8000;
        webResponse = webRequest.GetResponse();
      }
      catch
      {
      }
      if (webResponse == null)
        return (List<Eventos>) null;
      List<Eventos> eventosList = new List<Eventos>();
      Encoding encoding = Encoding.GetEncoding(1252);
      StreamReader streamReader = new StreamReader(webResponse.GetResponseStream(), encoding);
      string str1 = Regex.Replace(Regex.Replace(Regex.Replace(HttpUtility.HtmlDecode(Regex.Match(Regex.Match(streamReader.ReadToEnd().ToString(), "EVENTOS</div>([.\\s\\S]*<!-- eventos -->)", RegexOptions.IgnoreCase | RegexOptions.Multiline).Groups[1].Value.Trim(), "(<span style[.\\s\\S]*)<!-- eventos -->", RegexOptions.IgnoreCase | RegexOptions.Multiline).Groups[1].Value.Trim()).Replace("<br/>", Environment.NewLine).Replace("<br>", Environment.NewLine), "<a.*?href=[\"|']http://side(.*?)[\"|'].*?>(.*?)</a>", "{https://wssigacad.utad.pt/mobileUTAD/Root/ProxyURL?Url=http://side$1|$2}", RegexOptions.IgnoreCase | RegexOptions.Multiline), "<a.*?href=[\"|'](.*?)[\"|'].*?>(.*?)</a>", "{$1|$2}", RegexOptions.IgnoreCase | RegexOptions.Multiline), "#56688f;\">", "#56688f;\">H#");
      string str2 = "";
      int startIndex = 0;
      int num = 0;
      while (num != -1)
      {
        num = str1.IndexOf('\n', startIndex);
        if (num != -1)
        {
          string str3 = Regex.Replace(str1.Substring(startIndex, num - startIndex), "<.*?>", string.Empty).Trim() + Environment.NewLine;
          if (str3.StartsWith("H#") && str2 != "")
          {
            string input = str2.Replace("H#", "");
            string str4 = HttpUtility.HtmlDecode(Regex.Match(input, "^([\\w -\\/]+)\\s", RegexOptions.IgnoreCase | RegexOptions.Multiline).Groups[1].Value.Trim());
            eventosList.Add(new Eventos()
            {
              Title = str4.Replace("GERAL - ", ""),
              Description = input.Remove(0, str4.Length).Trim()
            });
            str2 = "";
          }
          str2 += str3;
          startIndex = num + 1;
        }
      }
      if (str2 != "")
      {
        string input = str2.Replace("H#", "");
        string str3 = HttpUtility.HtmlDecode(Regex.Match(input, "^([\\w -\\/]+)\\s", RegexOptions.IgnoreCase | RegexOptions.Multiline).Groups[1].Value.Trim());
        eventosList.Add(new Eventos()
        {
          Title = str3,
          Description = input.Remove(0, str3.Length).Trim()
        });
      }
      streamReader.Close();
      webResponse.Close();
      return eventosList;
    }
  }
}
