﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace mobileUTAD
{
    public static class WebConfigKeys
    {
        public static class ConnectionStrings
        {
        }

        public static class Keys
        {
            public static class WS
            {
                public static class Authentication
                {
                    public static string Username { get { return ConfigurationManager.AppSettings["WS_Auth_Username"].ToString(); } }
                    public static string Password { get { return ConfigurationManager.AppSettings["WS_Auth_Password"].ToString(); } }
                    public static string Domain { get { return ConfigurationManager.AppSettings["WS_Auth_Domain"].ToString(); } }
                }

            }
        }
    }
}
