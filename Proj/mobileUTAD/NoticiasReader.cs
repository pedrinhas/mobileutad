﻿// Decompiled with JetBrains decompiler
// Type: mobileUTAD.NoticiasReader
// Assembly: mobileUTAD, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C6335312-308C-477B-9BDC-E68404688049
// Assembly location: A:\Git repositories\mobileUTAD\bin\mobileUTAD.dll

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace mobileUTAD
{
  public class NoticiasReader
  {
    public static List<Noticias> Read(string url)
    {
      WebResponse webResponse = (WebResponse) null;
      try
      {
        WebRequest webRequest = WebRequest.Create(url);
        webRequest.Timeout = 8000;
        webResponse = webRequest.GetResponse();
      }
      catch
      {
      }
      if (webResponse == null)
        return (List<Noticias>) null;
      List<Noticias> noticiasList = new List<Noticias>();
      Encoding encoding = Encoding.GetEncoding(1252);
      StreamReader streamReader = new StreamReader(webResponse.GetResponseStream(), encoding);
      string str1 = Regex.Replace(Regex.Replace(Regex.Replace(HttpUtility.HtmlDecode(Regex.Match(Regex.Match(streamReader.ReadToEnd().ToString(), "AVISOS</div>([.\\s\\S]*<!-- noticias -->)", RegexOptions.IgnoreCase | RegexOptions.Multiline).Groups[1].Value.Trim(), "(<span style[.\\s\\S]*)<!-- noticias -->", RegexOptions.IgnoreCase | RegexOptions.Multiline).Groups[1].Value.Trim()).Replace("<br/>", Environment.NewLine).Replace("<br>", Environment.NewLine), "<a.*?href=[\"|']http://side(.*?)[\"|'].*?>(.*?)</a>", "{https://wssigacad.utad.pt/mobileUTAD/Root/ProxyURL?Url=http://side$1|$2}", RegexOptions.IgnoreCase | RegexOptions.Multiline), "<a.*?href=[\"|'](.*?)[\"|'].*?>(.*?)</a>", "{$1|$2}", RegexOptions.IgnoreCase | RegexOptions.Multiline), "#56688f;\">", "#56688f;\">H#");
      string str2 = "";
      int startIndex = 0;
      int num = 0;
      while (num != -1)
      {
        num = str1.IndexOf('\n', startIndex);
        if (num != -1)
        {
          string str3 = Regex.Replace(str1.Substring(startIndex, num - startIndex), "<.*?>", string.Empty).Trim() + Environment.NewLine;
          if (str3.StartsWith("H#") && str2 != "")
          {
            string input = str2.Replace("H#", "");
            Match match = Regex.Match(input, "{([^\\|]*)\\|(.*)}", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            if (match.Success)
              input = input.Replace(match.Groups[0].Value, "");
            string str4 = match.Groups[1].Value.Trim();
            string str5 = match.Groups[2].Value.Trim();
            string str6 = HttpUtility.HtmlDecode(Regex.Match(input, "^([\\w -\\/]+)\\s", RegexOptions.IgnoreCase | RegexOptions.Multiline).Groups[1].Value.Trim());
            Noticias noticias = new Noticias();
            noticias.Title = str6.Replace("GERAL - ", "");
            noticias.Description = input.Remove(0, str6.Length).Trim();
            if (str4 != "")
            {
              noticias.LinkUrl = str4;
              noticias.LinkName = str5;
            }
            else
              noticias.LinkUrl = "";
            noticiasList.Add(noticias);
            str2 = "";
          }
          str2 += str3;
          startIndex = num + 1;
        }
      }
      if (str2 != "")
      {
        string input = str2.Replace("H#", "");
        Match match = Regex.Match(input, "{([^\\|]*)\\|(.*)}", RegexOptions.IgnoreCase | RegexOptions.Multiline);
        if (match.Success)
          input = input.Replace(match.Groups[0].Value, "");
        string str3 = match.Groups[1].Value.Trim();
        string str4 = match.Groups[2].Value.Trim();
        string str5 = HttpUtility.HtmlDecode(Regex.Match(input, "^([\\w -\\/]+)\\s", RegexOptions.IgnoreCase | RegexOptions.Multiline).Groups[1].Value.Trim());
        Noticias noticias = new Noticias();
        noticias.Title = str5.Replace("GERAL - ", "");
        noticias.Description = input.Remove(0, str5.Length).Trim();
        if (str3 != "")
        {
          noticias.LinkUrl = str3;
          noticias.LinkName = str4;
        }
        else
          noticias.LinkUrl = "";
        noticiasList.Add(noticias);
      }
      streamReader.Close();
      webResponse.Close();
      return noticiasList;
    }

    public static string GetUnicodeString(string s)
    {
      s = new string(s.SelectMany<char, char>((Func<char, IEnumerable<char>>) (c =>
      {
        if ((int) c > (int) sbyte.MaxValue)
          return (IEnumerable<char>) ("\\u" + ((int) c).ToString("X4")).ToArray<char>();
        return (IEnumerable<char>) new char[1]{ c };
      })).ToArray<char>());
      return s;
    }
  }
}
