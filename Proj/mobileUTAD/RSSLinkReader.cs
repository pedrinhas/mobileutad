﻿// Decompiled with JetBrains decompiler
// Type: mobileUTAD.RSSLinkReader
// Assembly: mobileUTAD, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C6335312-308C-477B-9BDC-E68404688049
// Assembly location: A:\Git repositories\mobileUTAD\bin\mobileUTAD.dll

using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace mobileUTAD
{
  public class RSSLinkReader
  {
    public static string Read(string url, string nomeCurso)
    {
      WebResponse webResponse = (WebResponse) null;
      try
      {
        WebRequest webRequest = WebRequest.Create(url);
        webRequest.Timeout = 5000;
        webResponse = webRequest.GetResponse();
      }
      catch
      {
      }
      if (webResponse == null)
        return (string) null;
      Encoding encoding = Encoding.GetEncoding(1252);
      StreamReader streamReader = new StreamReader(webResponse.GetResponseStream(), encoding);
      string input = HttpUtility.HtmlDecode(streamReader.ReadToEnd().ToString());
      string str = string.Format("{0}|{1}", (object) Regex.Match(input, "<a href=\"([^\"]*)\"\\s+title=\"" + nomeCurso, RegexOptions.IgnoreCase | RegexOptions.Multiline).Groups[1].Value.Trim(), (object) Regex.Match(input, nomeCurso + "</a><a href=\"([^\"]*)\"", RegexOptions.IgnoreCase | RegexOptions.Multiline).Groups[1].Value.Trim());
      streamReader.Close();
      webResponse.Close();
      return str;
    }
  }
}
