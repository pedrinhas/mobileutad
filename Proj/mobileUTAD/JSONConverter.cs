﻿// Decompiled with JetBrains decompiler
// Type: mobileUTAD.JSONConverter
// Assembly: mobileUTAD, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C6335312-308C-477B-9BDC-E68404688049
// Assembly location: A:\Git repositories\mobileUTAD\bin\mobileUTAD.dll

using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;

namespace mobileUTAD
{
  public static class JSONConverter
  {
    public static string ToJSON<T>(this T obj) where T : class
    {
      using (MemoryStream memoryStream = new MemoryStream())
      {
        new DataContractJsonSerializer(typeof (T)).WriteObject((Stream) memoryStream, (object) obj);
        return Encoding.UTF8.GetString(memoryStream.ToArray());
      }
    }

    public static T FromJSON<T>(this T obj, string json) where T : class
    {
      using (MemoryStream memoryStream = new MemoryStream(Encoding.Unicode.GetBytes(json)))
        return new DataContractJsonSerializer(typeof (T)).ReadObject((Stream) memoryStream) as T;
    }
  }
}
