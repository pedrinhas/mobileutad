﻿// Decompiled with JetBrains decompiler
// Type: mobileUTAD.Global
// Assembly: mobileUTAD, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C6335312-308C-477B-9BDC-E68404688049
// Assembly location: A:\Git repositories\mobileUTAD\bin\mobileUTAD.dll

using System;
using System.ServiceModel.Activation;
using System.Web;
using System.Web.Routing;

namespace mobileUTAD
{
  public class Global : HttpApplication
  {
    protected void Application_Start(object sender, EventArgs e)
    {
      RouteTable.Routes.Add((RouteBase) new ServiceRoute("Root", (ServiceHostFactoryBase) new WebServiceHostFactory(), typeof (ServiceMobile)));
    }

    protected void Session_Start(object sender, EventArgs e)
    {
      string sessionId = this.Session.SessionID;
    }

    protected void Application_BeginRequest(object sender, EventArgs e)
    {
    }

    private void EnableCrossDomainAjaxCall()
    {
    }

    protected void Application_AuthenticateRequest(object sender, EventArgs e)
    {
    }

    protected void Application_Error(object sender, EventArgs e)
    {
    }

    protected void Session_End(object sender, EventArgs e)
    {
    }

    protected void Application_End(object sender, EventArgs e)
    {
    }
  }
}
