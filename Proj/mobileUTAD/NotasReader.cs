﻿// Decompiled with JetBrains decompiler
// Type: mobileUTAD.NotasReader
// Assembly: mobileUTAD, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C6335312-308C-477B-9BDC-E68404688049
// Assembly location: A:\Git repositories\mobileUTAD\bin\mobileUTAD.dll

using mobileUTAD.SIGACADClient;
using System.Collections.Generic;

namespace mobileUTAD
{
  public class NotasReader
  {
    public static PlanoCurricular Read(string numAluno, mobileUTAD.SIGACADClient.SIGACADClient sigacad)
    {
      PlanoCurricular planoCurricular = new PlanoCurricular();
      List<Disciplina> disciplinaList = new List<Disciplina>();
      Curso curso = new Curso();
      List<Curso> cursoList = new List<Curso>();
      List<alunoPlanoCurricular> alunoPlanoCurricularList = new List<alunoPlanoCurricular>();
      if (alunoPlanoCurricularList != null)
      {
        alunoPlanoCurricularList.AddRange((IEnumerable<alunoPlanoCurricular>) sigacad.GetAlunoNotas(numAluno));
        string str = "";
        foreach (alunoPlanoCurricular alunoPlanoCurricular in alunoPlanoCurricularList)
        {
          if (alunoPlanoCurricular.curso.ToString() != str)
          {
            str = alunoPlanoCurricular.curso;
            curso.Nome = str;
            if (disciplinaList.Count > 0)
            {
              curso.Disciplinas = disciplinaList;
              cursoList.Add(curso);
              curso = new Curso();
              disciplinaList = new List<Disciplina>();
            }
          }
          disciplinaList.Add(new Disciplina()
          {
            Nome = alunoPlanoCurricular.disciplina + (alunoPlanoCurricular.disciplina.Contains("OPÇÃO") ? " (" + alunoPlanoCurricular.IP + ")" : ""),
            Nota = alunoPlanoCurricular.nota == "" ? "-" : alunoPlanoCurricular.nota,
            Posicao = string.Format("[A{0}/S{1}]", (object) alunoPlanoCurricular.ano, (object) alunoPlanoCurricular.semestre)
          });
        }
        curso.Disciplinas = disciplinaList;
        cursoList.Add(curso);
        List<AlunoSIGACAD> alunoSigacadList = new List<AlunoSIGACAD>();
        if (alunoSigacadList != null)
        {
          try
          {
            AlunoSIGACAD[] alunoDadosPessoais = sigacad.GetAlunoDadosPessoais(numAluno);
            if (alunoDadosPessoais != null)
            {
              alunoSigacadList.AddRange((IEnumerable<AlunoSIGACAD>) alunoDadosPessoais);
              foreach (AlunoSIGACAD alunoSigacad in alunoSigacadList)
              {
                planoCurricular.Numero = alunoSigacad.NumProcesso;
                planoCurricular.Nome = alunoSigacad.Nome;
                planoCurricular.Regime = alunoSigacad.nomeRegime;
              }
            }
          }
          catch
          {
          }
        }
        planoCurricular.Curso = cursoList;
      }
      return planoCurricular;
    }
  }
}
