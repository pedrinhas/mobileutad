﻿// Decompiled with JetBrains decompiler
// Type: mobileUTAD.ServiceMobile
// Assembly: mobileUTAD, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C6335312-308C-477B-9BDC-E68404688049
// Assembly location: A:\Git repositories\mobileUTAD\bin\mobileUTAD.dll

using System;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Security.Principal;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;

namespace mobileUTAD
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, InstanceContextMode = InstanceContextMode.Single)]
    public class ServiceMobile : IServiceMobile
    {
        private static Regex nonAsciiCodepoints = new Regex("[\"]|[^\\x20-\\x7f]");
        private mobileUTAD.SIGACADClient.SIGACADClient sigacad = (mobileUTAD.SIGACADClient.SIGACADClient)null;
        private double duracaoHoras;
        private string userID;
        private string numAluno;
        private string nomeCurso;

        public string UserID
        {
            get
            {
                return this.userID;
            }
            set
            {
                this.userID = value;
            }
        }

        public string NumAluno
        {
            get
            {
                return this.numAluno;
            }
            set
            {
                this.numAluno = value;
            }
        }

        public string NomeCurso
        {
            get
            {
                return this.nomeCurso;
            }
            set
            {
                this.nomeCurso = value;
            }
        }

        public Stream Decode64()
        {
            WebOperationContext.Current.OutgoingResponse.Format = new WebMessageFormat?(WebMessageFormat.Json);
            WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain; charset=utf-8";
            return (Stream)new MemoryStream(Encoding.Default.GetBytes(this.DecodeBase64(this.Deobfuscate(WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["Encoded"] ?? ""))));
        }

        public Stream Hello()
        {
            WebOperationContext.Current.OutgoingResponse.Format = new WebMessageFormat?(WebMessageFormat.Json);
            WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain; charset=utf-8";
            return (Stream)new MemoryStream(Encoding.Default.GetBytes("Olá, isto é um teste..."));
        }

        public Stream ProxyURL()
        {
            string requestUriString = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["Url"] ?? "";
            Stream stream = (Stream)null;
            if (requestUriString != "")
            {
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(requestUriString);
                httpWebRequest.Referer = "http://side.utad.pt/";
                HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse();
                stream = response.GetResponseStream();
                WebOperationContext.Current.OutgoingResponse.Headers[System.Net.HttpResponseHeader.ContentType] = response.Headers[System.Net.HttpResponseHeader.ContentType];
            }
            return stream;
        }

        public List<CalendarioAcademico> CalendarioAcademico()
        {
            string token = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["Token"] ?? "";
            string str1 = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["Curso"] ?? "";
            WebOperationContext.Current.OutgoingResponse.Format = new WebMessageFormat?(WebMessageFormat.Json);
            this.NomeCurso = !(str1 == "") ? str1 : this.TokenValues(token)["curso"];
            bool flag = false;
            string link = this.GetLink(this.NomeCurso, ServiceMobile.LinkType.CURSO);
            string str2 = HttpContext.Current.Server.MapPath(string.Format("~/App_Data/cal_{0}.json", (object)link.Split('/')[1]));
            List<CalendarioAcademico> calendarioAcademicoList = new List<CalendarioAcademico>();
            if (this.IsFileValid(str2) && this.IsFileCached(str2, 168.0))
            {
                calendarioAcademicoList = new JavaScriptSerializer().Deserialize<List<CalendarioAcademico>>(this.ReadTextFile(str2));
                flag = true;
            }
            if (!flag)
            {
                string[] strArray = new string[3]
        {
          "eventos",
          "aulas",
          "avaliacoes"
        };
                for (int index = 0; index < 3; ++index)
                {
                    List<Calendario> calendarioList1 = new List<Calendario>();
                    List<Calendario> calendarioList2 = CalendarioEventosReader.Read(string.Format("http://side.utad.pt/{0}/calendario/{1}/", (object)link, (object)strArray[index]));
                    if (calendarioList2 != null)
                        calendarioAcademicoList.Add(new CalendarioAcademico()
                        {
                            Title = strArray[index],
                            Cal = calendarioList2
                        });
                }
                if (calendarioAcademicoList != null)
                {
                    MemoryStream memoryStream = new MemoryStream();
                    new DataContractJsonSerializer(typeof(List<CalendarioAcademico>)).WriteObject((Stream)memoryStream, (object)calendarioAcademicoList);
                    memoryStream.Position = 0L;
                    string end = new StreamReader((Stream)memoryStream).ReadToEnd();
                    this.WriteTextFile(str2, end);
                }
                else if (this.IsFileValid(str2))
                    calendarioAcademicoList = new JavaScriptSerializer().Deserialize<List<CalendarioAcademico>>(this.ReadTextFile(str2));
            }
            return calendarioAcademicoList;
        }

        public List<Calendario> CalendarioEventos()
        {
            string token = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["Token"] ?? "";
            WebOperationContext.Current.OutgoingResponse.Format = new WebMessageFormat?(WebMessageFormat.Json);
            this.NomeCurso = this.TokenValues(token)["curso"];
            bool flag = false;
            string link = this.GetLink(this.NomeCurso, ServiceMobile.LinkType.CURSO);
            string str = HttpContext.Current.Server.MapPath(string.Format("~/App_Data/cev_{0}.json", (object)link.Split('/')[1]));
            List<Calendario> calendarioList = new List<Calendario>();
            if (this.IsFileValid(str) && this.IsFileCached(str, 24.0))
            {
                calendarioList = new JavaScriptSerializer().Deserialize<List<Calendario>>(this.ReadTextFile(str));
                flag = true;
            }
            if (!flag)
            {
                calendarioList = CalendarioEventosReader.Read(string.Format("http://side.utad.pt/{0}/calendario/eventos/", (object)link));
                if (calendarioList != null)
                {
                    MemoryStream memoryStream = new MemoryStream();
                    new DataContractJsonSerializer(typeof(List<Calendario>)).WriteObject((Stream)memoryStream, (object)calendarioList);
                    memoryStream.Position = 0L;
                    string end = new StreamReader((Stream)memoryStream).ReadToEnd();
                    this.WriteTextFile(str, end);
                }
                else if (this.IsFileValid(str))
                    calendarioList = new JavaScriptSerializer().Deserialize<List<Calendario>>(this.ReadTextFile(str));
            }
            return calendarioList;
        }

        public List<Calendario> CalendarioAulas()
        {
            string token = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["Token"] ?? "";
            WebOperationContext.Current.OutgoingResponse.Format = new WebMessageFormat?(WebMessageFormat.Json);
            this.NomeCurso = this.TokenValues(token)["curso"];
            bool flag = false;
            string link = this.GetLink(this.NomeCurso, ServiceMobile.LinkType.CURSO);
            string str = HttpContext.Current.Server.MapPath(string.Format("~/App_Data/cau_{0}.json", (object)link.Split('/')[1]));
            List<Calendario> calendarioList = new List<Calendario>();
            if (this.IsFileValid(str) && this.IsFileCached(str, 24.0))
            {
                calendarioList = new JavaScriptSerializer().Deserialize<List<Calendario>>(this.ReadTextFile(str));
                flag = true;
            }
            if (!flag)
            {
                calendarioList = CalendarioAulasReader.Read(string.Format("http://side.utad.pt/{0}/calendario/aulas/", (object)link));
                if (calendarioList != null)
                {
                    MemoryStream memoryStream = new MemoryStream();
                    new DataContractJsonSerializer(typeof(List<Calendario>)).WriteObject((Stream)memoryStream, (object)calendarioList);
                    memoryStream.Position = 0L;
                    string end = new StreamReader((Stream)memoryStream).ReadToEnd();
                    this.WriteTextFile(str, end);
                }
                else if (this.IsFileValid(str))
                    calendarioList = new JavaScriptSerializer().Deserialize<List<Calendario>>(this.ReadTextFile(str));
            }
            return calendarioList;
        }

        public List<Calendario> CalendarioAvaliacoes()
        {
            string token = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["Token"] ?? "";
            WebOperationContext.Current.OutgoingResponse.Format = new WebMessageFormat?(WebMessageFormat.Json);
            this.NomeCurso = this.TokenValues(token)["curso"];
            bool flag = false;
            string link = this.GetLink(this.NomeCurso, ServiceMobile.LinkType.CURSO);
            string str = HttpContext.Current.Server.MapPath(string.Format("~/App_Data/cav_{0}.json", (object)link.Split('/')[1]));
            List<Calendario> calendarioList = new List<Calendario>();
            if (this.IsFileValid(str) && this.IsFileCached(str, 24.0))
            {
                calendarioList = new JavaScriptSerializer().Deserialize<List<Calendario>>(this.ReadTextFile(str));
                flag = true;
            }
            if (!flag)
            {
                calendarioList = CalendarioAvaliacoesReader.Read(string.Format("http://side.utad.pt/{0}/calendario/avaliacoes/", (object)link));
                if (calendarioList != null)
                {
                    MemoryStream memoryStream = new MemoryStream();
                    new DataContractJsonSerializer(typeof(List<Calendario>)).WriteObject((Stream)memoryStream, (object)calendarioList);
                    memoryStream.Position = 0L;
                    string end = new StreamReader((Stream)memoryStream).ReadToEnd();
                    this.WriteTextFile(str, end);
                }
                else if (this.IsFileValid(str))
                    calendarioList = new JavaScriptSerializer().Deserialize<List<Calendario>>(this.ReadTextFile(str));
            }
            return calendarioList;
        }

        public List<Eventos> Eventos()
        {
            string token = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["Token"] ?? "";
            WebOperationContext.Current.OutgoingResponse.Format = new WebMessageFormat?(WebMessageFormat.Json);
            this.NomeCurso = this.TokenValues(token)["curso"];
            bool flag = false;
            string str = HttpContext.Current.Server.MapPath("~/App_Data/eventos.json");
            List<Eventos> eventosList = new List<Eventos>();
            if (this.IsFileValid(str) && this.IsFileCached(str, 12.0))
            {
                eventosList = new JavaScriptSerializer().Deserialize<List<Eventos>>(this.ReadTextFile(str));
                flag = true;
            }
            if (!flag)
            {
                eventosList = EventosReader.Read("http://side.utad.pt/");
                if (eventosList != null)
                {
                    MemoryStream memoryStream = new MemoryStream();
                    new DataContractJsonSerializer(typeof(List<Eventos>)).WriteObject((Stream)memoryStream, (object)eventosList);
                    memoryStream.Position = 0L;
                    string end = new StreamReader((Stream)memoryStream).ReadToEnd();
                    this.WriteTextFile(str, end);
                }
                else if (this.IsFileValid(str))
                    eventosList = new JavaScriptSerializer().Deserialize<List<Eventos>>(this.ReadTextFile(str));
            }
            return eventosList;
        }

        public List<Noticias> Noticias()
        {
            string token = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["Token"] ?? "";
            WebOperationContext.Current.OutgoingResponse.Format = new WebMessageFormat?(WebMessageFormat.Json);
            this.NomeCurso = this.TokenValues(token)["curso"];
            bool flag = false;
            string str = HttpContext.Current.Server.MapPath("~/App_Data/noticias.json");
            List<Noticias> noticiasList = new List<Noticias>();
            if (this.IsFileValid(str) && this.IsFileCached(str, 12.0))
            {
                noticiasList = new JavaScriptSerializer().Deserialize<List<Noticias>>(this.ReadTextFile(str));
                flag = true;
            }
            if (!flag)
            {
                noticiasList = NoticiasReader.Read("http://side.utad.pt/");
                if (noticiasList != null)
                {
                    MemoryStream memoryStream = new MemoryStream();
                    new DataContractJsonSerializer(typeof(List<Noticias>)).WriteObject((Stream)memoryStream, (object)noticiasList);
                    memoryStream.Position = 0L;
                    string end = new StreamReader((Stream)memoryStream).ReadToEnd();
                    this.WriteTextFile(str, end);
                }
                else if (this.IsFileValid(str))
                    noticiasList = new JavaScriptSerializer().Deserialize<List<Noticias>>(this.ReadTextFile(str));
            }
            return noticiasList;
        }

        private string GetLink(string nomeCurso, ServiceMobile.LinkType linkType)
        {
            string str1 = "";
            nomeCurso = nomeCurso == "-" ? "ENGENHARIA INFORMÁTICA (1º CICLO)" : nomeCurso;
            string str2 = HttpContext.Current.Server.MapPath("~/App_Data/RSSlinks.json");
            Dictionary<string, string> dictionary1 = new Dictionary<string, string>();
            if (this.IsFileValid(str2))
            {
                string input = this.ReadTextFile(str2);
                if (input != "" && input != null)
                {
                    Dictionary<string, string> dictionary2 = new JavaScriptSerializer().Deserialize<Dictionary<string, string>>(input);
                    if (dictionary2.ContainsKey(nomeCurso))
                        str1 = dictionary2[nomeCurso];
                }
            }
            string[] strArray = str1.Split('|');
            return linkType != ServiceMobile.LinkType.RSS ? strArray[0] : strArray[1];
        }

        public List<Rss> Avisos()
        {
            string token = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["Token"] ?? "";
            string str1 = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["Curso"] ?? "";
            WebOperationContext.Current.OutgoingResponse.Format = new WebMessageFormat?(WebMessageFormat.Json);
            this.NomeCurso = !(str1 == "") ? str1 : this.TokenValues(token)["curso"];
            List<Rss> rssList = new List<Rss>();
            if (this.nomeCurso != "")
            {
                string link = this.GetLink(this.nomeCurso, ServiceMobile.LinkType.RSS);
                if (link != "" && link != null)
                {
                    Match match = new Regex("\\?(.*)$").Match(link);
                    if (match.Success)
                    {
                        string str2 = HttpContext.Current.Server.MapPath("~/App_Data/" + match.Groups[1].Value + ".json");
                        bool flag = false;
                        if (this.IsFileValid(str2) && this.IsFileCached(str2))
                        {
                            rssList = new JavaScriptSerializer().Deserialize<List<Rss>>(this.ReadTextFile(str2));
                            flag = true;
                        }
                        if (!flag)
                        {
                            rssList = RssReader.Read("http://side.utad.pt/" + link);
                            if (rssList != null)
                            {
                                MemoryStream memoryStream = new MemoryStream();
                                new DataContractJsonSerializer(typeof(List<Rss>)).WriteObject((Stream)memoryStream, (object)rssList);
                                memoryStream.Position = 0L;
                                string end = new StreamReader((Stream)memoryStream).ReadToEnd();
                                this.WriteTextFile(str2, end);
                            }
                            else if (this.IsFileValid(str2))
                                rssList = new JavaScriptSerializer().Deserialize<List<Rss>>(this.ReadTextFile(str2));
                        }
                    }
                }
            }
            return rssList;
        }

        public Aluno DadosPessoais()
        {
            string userAluno = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["User"] ?? "";
            string passAluno = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["Pass"] ?? "";
            string token = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["Token"] ?? "";
            WebOperationContext.Current.OutgoingResponse.Format = new WebMessageFormat?(WebMessageFormat.Json);
            Aluno aluno = (Aluno)null;
            if (this.Autenticar(userAluno, passAluno, token))
                aluno = DadosPessoaisReader.Read(this.NumAluno, this.sigacad);
            return aluno;
        }

        public PlanoCurricular Notas()
        {
            string userAluno = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["User"] ?? "";
            string passAluno = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["Pass"] ?? "";
            string token = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["Token"] ?? "";
            WebOperationContext.Current.OutgoingResponse.Format = new WebMessageFormat?(WebMessageFormat.Json);
            PlanoCurricular planoCurricular = (PlanoCurricular)null;
            if (this.Autenticar(userAluno, passAluno, token))
                planoCurricular = NotasReader.Read(this.NumAluno, this.sigacad);
            return planoCurricular;
        }

        public PlanoCurricular Inscricoes()
        {
            string userAluno = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["User"] ?? "";
            string passAluno = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["Pass"] ?? "";
            string token = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["Token"] ?? "";
            WebOperationContext.Current.OutgoingResponse.Format = new WebMessageFormat?(WebMessageFormat.Json);
            PlanoCurricular planoCurricular = (PlanoCurricular)null;
            if (this.Autenticar(userAluno, passAluno, token))
                planoCurricular = InscricoesReader.Read(this.NumAluno, this.sigacad);
            return planoCurricular;
        }

        public Propinas Propinas()
        {
            string userAluno = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["User"] ?? "";
            string passAluno = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["Pass"] ?? "";
            string token = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["Token"] ?? "";
            WebOperationContext.Current.OutgoingResponse.Format = new WebMessageFormat?(WebMessageFormat.Json);
            Propinas propinas = (Propinas)null;
            if (this.Autenticar(userAluno, passAluno, token))
                propinas = PropinasReader.Read(this.NumAluno, this.sigacad);
            return propinas;
        }

        public PlanoDividas Dividas()
        {
            string userAluno = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["User"] ?? "";
            string passAluno = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["Pass"] ?? "";
            string token = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["Token"] ?? "";
            WebOperationContext.Current.OutgoingResponse.Format = new WebMessageFormat?(WebMessageFormat.Json);
            PlanoDividas planoDividas = (PlanoDividas)null;
            if (this.Autenticar(userAluno, passAluno, token))
                planoDividas = DividasReader.Read(this.NumAluno, this.sigacad);
            return planoDividas;
        }

        public Propinas Referencias()
        {
            string userAluno = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["User"] ?? "";
            string passAluno = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["Pass"] ?? "";
            string token = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters["Token"] ?? "";
            WebOperationContext.Current.OutgoingResponse.Format = new WebMessageFormat?(WebMessageFormat.Json);
            Propinas propinas = (Propinas)null;
            if (this.Autenticar(userAluno, passAluno, token))
                propinas = ReferenciasReader.Read(this.NumAluno, this.sigacad);
            return propinas;
        }

        public bool Autenticar(string userAluno, string passAluno, string token)
        {
            bool flag = false;
            if (token != "")
            {
                Dictionary<string, string> dictionary = this.TokenValues(token);
                userAluno = dictionary["user"];
                passAluno = dictionary["pass"];
                this.NomeCurso = dictionary["curso"];
                this.UserID = dictionary["id"];
            }
            else
            {
                this.NomeCurso = "";
                this.UserID = DateTime.Now.ToString("yyyyMMddHHmmss");
            }
            Match match = new Regex("^\\D?\\D?\\D?([0-9]+)").Match(userAluno);
            if (match.Success)
            {
                this.NumAluno = match.Groups[1].Value;
                if (this.AutenticarAluno(userAluno, passAluno))
                {
                    this.sigacad = new mobileUTAD.SIGACADClient.SIGACADClient();
                    #region COM AUTH
                    //this.sigacad.ClientCredentials.Windows.ClientCredential = new NetworkCredential(WebConfigKeys.Keys.WS.Authentication.Username, WebConfigKeys.Keys.WS.Authentication.Password, WebConfigKeys.Keys.WS.Authentication.Domain);
                    #endregion
                    //this.sigacad.ClientCredentials.Windows.ClientCredential = new NetworkCredential("wsuser", "$3Cdvfbg3#", "inqueritos");
                    this.sigacad.ClientCredentials.Windows.AllowedImpersonationLevel = TokenImpersonationLevel.Impersonation;
                    flag = true;
                }
            }
            return flag;
        }

        public bool AutenticarAluno(string userAluno, string passAluno)
        {
            bool flag = false;
            if (userAluno.StartsWith("el") && passAluno == userAluno)
                return true;
            if (passAluno != "")
            {
                try
                {
                    string str = OperationContext.Current.Channel.LocalAddress.ToString();
                    flag = new DirectorySearcher(!str.Contains("localhost") ? new DirectoryEntry(string.Format("LDAP://{0}/OU=alunos,OU=organizacao,DC=utad,DC=pt", ConfigurationManager.AppSettings["LDAPAlunosURL"]), "UID=" + userAluno + ",OU=alunos,OU=organizacao,DC=utad,DC=pt", passAluno, AuthenticationTypes.ServerBind) : new DirectoryEntry("LDAP://panoias.utad.pt/OU=alunos,OU=organizacao,DC=utad,DC=pt", "UID=" + userAluno + ",OU=alunos,OU=organizacao,DC=utad,DC=pt", passAluno, AuthenticationTypes.ServerBind), "(UID=" + userAluno + ")").FindOne() != null;
                }
                catch
                {
                    flag = false;
                }
            }
            return flag;
        }

        private Dictionary<string, string> TokenValues(string token)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            string[] strArray = this.DecodeBase64(this.Deobfuscate(token)).Split('|');
            if (strArray.Length == 4)
            {
                dictionary.Add("user", strArray[0]);
                dictionary.Add("pass", strArray[1]);
                dictionary.Add("curso", strArray[2]);
                dictionary.Add("id", strArray[3]);
            }
            return dictionary;
        }

        public string DecodeBase64(string encoded)
        {
            return Encoding.UTF8.GetString(Convert.FromBase64String(encoded));
        }

        public string EncodeBase64(string toEncode)
        {
            return Convert.ToBase64String(Encoding.Unicode.GetBytes(toEncode));
        }

        public string Deobfuscate(string s)
        {
            string str = "";
            int startIndex = 0;
            while (startIndex < s.Length)
            {
                str = str + s.Substring(startIndex + 1, 1) + s.Substring(startIndex, 1);
                startIndex += 2;
            }
            return str;
        }

        public void WriteTextFile(string filename, string contents)
        {
            TextWriter textWriter = (TextWriter)new StreamWriter(filename);
            textWriter.WriteLine(contents);
            textWriter.Close();
        }

        public string ReadTextFile(string filename)
        {
            TextReader textReader = (TextReader)new StreamReader(filename);
            string str = textReader.ReadLine();
            textReader.Close();
            return str;
        }

        private bool IsFileValid(string filePath)
        {
            bool flag = false;
            if (System.IO.File.Exists(filePath))
                flag = true;
            return flag;
        }

        private bool IsFileCached(string filePath)
        {
            return this.IsFileCached(filePath, 1.0);
        }

        private bool IsFileCached(string filePath, double horas)
        {
            bool flag = false;
            TimeSpan timeSpan = DateTime.Now - System.IO.File.GetLastWriteTime(filePath);
            this.duracaoHoras = timeSpan.TotalHours;
            if (timeSpan.TotalHours <= horas)
                flag = true;
            return flag;
        }

        private static string encodeStringValue(string value)
        {
            return ServiceMobile.nonAsciiCodepoints.Replace(value, new MatchEvaluator(ServiceMobile.encodeSingleChar));
        }

        private static string encodeSingleChar(Match match)
        {
            return "\\u" + char.ConvertToUtf32(match.Value, 0).ToString("x4");
        }

        private enum LinkType
        {
            RSS,
            CURSO,
        }
    }
}
