﻿// Decompiled with JetBrains decompiler
// Type: mobileUTAD.RssReader
// Assembly: mobileUTAD, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C6335312-308C-477B-9BDC-E68404688049
// Assembly location: A:\Git repositories\mobileUTAD\bin\mobileUTAD.dll

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace mobileUTAD
{
  public class RssReader
  {
    public static List<Rss> Read(string url)
    {
      WebResponse webResponse = (WebResponse) null;
      try
      {
        WebRequest webRequest = WebRequest.Create(url);
        webRequest.Timeout = 8000;
        webResponse = webRequest.GetResponse();
      }
      catch
      {
      }
      if (webResponse == null)
        return (List<Rss>) null;
      DataSet dataSet = new DataSet();
      int num = (int) dataSet.ReadXml(webResponse.GetResponseStream());
      List<Rss> rssList = new List<Rss>();
      if (dataSet.Tables.Count > 3)
        rssList = dataSet.Tables["item"].AsEnumerable().Select<DataRow, Rss>((Func<DataRow, Rss>) (row => new Rss()
        {
          Title = row.Field<string>("title").Replace("<br/>", Environment.NewLine).Replace("<br>", Environment.NewLine).Trim(),
          PublicationDate = row.Field<string>("pubDate"),
          Description = row.Field<string>("description").Replace("<br/>", Environment.NewLine).Replace("<br>", Environment.NewLine).Trim()
        })).ToList<Rss>();
      foreach (Rss rss in rssList)
      {
        rss.Description = !rss.Description.Contains("http://side.utad.pt") ? Regex.Replace(rss.Description, "<a.*?href=[\"|'](.*?)[\"|'].*?>(.*?)</a>", "{$1|$2}", RegexOptions.IgnoreCase | RegexOptions.Multiline) : Regex.Replace(rss.Description, "<a.*?href=[\"|'](.*?)[\"|'].*?>(.*?)</a>", "{https://wssigacad.utad.pt/mobileUTAD/Root/ProxyURL?Url=$1|$2}", RegexOptions.IgnoreCase | RegexOptions.Multiline);
        rss.Description = Regex.Replace(rss.Description, "<.*?>", string.Empty).Trim() + Environment.NewLine;
      }
      return rssList;
    }
  }
}
