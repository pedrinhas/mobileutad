﻿// Decompiled with JetBrains decompiler
// Type: mobileUTAD.Referencia
// Assembly: mobileUTAD, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C6335312-308C-477B-9BDC-E68404688049
// Assembly location: A:\Git repositories\mobileUTAD\bin\mobileUTAD.dll

namespace mobileUTAD
{
  public class Referencia
  {
    public string Ano { get; set; }

    public string DataLimite { get; set; }

    public string Descricao { get; set; }

    public string Entidade { get; set; }

    public string Ref { get; set; }

    public string Prestacao { get; set; }

    public string Valor { get; set; }
  }
}
