﻿// Decompiled with JetBrains decompiler
// Type: mobileUTAD.Divida
// Assembly: mobileUTAD, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C6335312-308C-477B-9BDC-E68404688049
// Assembly location: A:\Git repositories\mobileUTAD\bin\mobileUTAD.dll

namespace mobileUTAD
{
  public class Divida
  {
    public string Data { get; set; }

    public string Descricao { get; set; }

    public string Tipo { get; set; }

    public string Valor { get; set; }
  }
}
