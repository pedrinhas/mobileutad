﻿// Decompiled with JetBrains decompiler
// Type: mobileUTAD.CalendarioEventosReader
// Assembly: mobileUTAD, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C6335312-308C-477B-9BDC-E68404688049
// Assembly location: A:\Git repositories\mobileUTAD\bin\mobileUTAD.dll

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace mobileUTAD
{
  public class CalendarioEventosReader
  {
    public static List<Calendario> Read(string url)
    {
      WebResponse webResponse = (WebResponse) null;
      try
      {
        WebRequest webRequest = WebRequest.Create(url);
        webRequest.Timeout = 8000;
        webResponse = webRequest.GetResponse();
      }
      catch
      {
      }
      if (webResponse == null)
        return (List<Calendario>) null;
      List<Calendario> calendarioList = new List<Calendario>();
      Encoding encoding = Encoding.GetEncoding(1252);
      StreamReader streamReader = new StreamReader(webResponse.GetResponseStream(), encoding);
      string str1 = Regex.Replace(Regex.Replace(HttpUtility.HtmlDecode(Regex.Match(streamReader.ReadToEnd(), "content_side\"></a>([.\\s\\S]*)<h3>Calendário da res", RegexOptions.IgnoreCase | RegexOptions.Multiline).Groups[1].Value.Trim()), "<br><br>[\\s]*", Environment.NewLine, RegexOptions.IgnoreCase | RegexOptions.Multiline), "<br>[\\s]*", "#", RegexOptions.IgnoreCase | RegexOptions.Multiline) + Environment.NewLine;
      string input = "";
      int startIndex = 0;
      int num = 0;
      while (num != -1)
      {
        num = str1.IndexOf('\n', startIndex);
        if (num != -1)
        {
          string str2 = Regex.Replace(str1.Substring(startIndex, num - startIndex), "<.*?>", string.Empty).Trim() + Environment.NewLine;
          if (input != "")
          {
            string[] strArray = HttpUtility.HtmlDecode(Regex.Match(input, "^([\\w -\\/]+)\\s", RegexOptions.IgnoreCase | RegexOptions.Multiline).Groups[1].Value).Split('#');
            calendarioList.Add(new Calendario()
            {
              Title = strArray[0].Trim(),
              Description = strArray[1].Trim()
            });
            input = "";
          }
          input += str2;
          startIndex = num + 1;
        }
      }
      streamReader.Close();
      webResponse.Close();
      return calendarioList;
    }
  }
}
