﻿// Decompiled with JetBrains decompiler
// Type: mobileUTAD.SIGACADClient.AlunoSIGACAD
// Assembly: mobileUTAD, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C6335312-308C-477B-9BDC-E68404688049
// Assembly location: A:\Git repositories\mobileUTAD\bin\mobileUTAD.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace mobileUTAD.SIGACADClient
{
  [DebuggerStepThrough]
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DataContract(Name = "AlunoSIGACAD", Namespace = "http://schemas.datacontract.org/2004/07/wsSIGACAD2")]
  [Serializable]
  public class AlunoSIGACAD : IExtensibleDataObject, INotifyPropertyChanged
  {
    [NonSerialized]
    private ExtensionDataObject extensionDataField;
    [OptionalField]
    private string AltDataField;
    [OptionalField]
    private string AltUserField;
    [OptionalField]
    private string AlunoDeslocadoField;
    [OptionalField]
    private string AnoField;
    [OptionalField]
    private string AnoCurricularField;
    [OptionalField]
    private string BolseiroField;
    [OptionalField]
    private string Bolseiro_InstField;
    [OptionalField]
    private string CicloField;
    [OptionalField]
    private string CodPostalField;
    [OptionalField]
    private string CursoField;
    [OptionalField]
    private DateTime DataNascField;
    [OptionalField]
    private string EMailField;
    [OptionalField]
    private string EnderecoField;
    [OptionalField]
    private string EstabField;
    [OptionalField]
    private string EstadoCivilField;
    [OptionalField]
    private string EstudanteTrabalhadorField;
    [OptionalField]
    private string FormaIngressoField;
    [OptionalField]
    private string FreqOutrosField;
    [OptionalField]
    private string HabilAnt_AnoConcField;
    [OptionalField]
    private string HabilAnt_PaisField;
    [OptionalField]
    private string HabilAnt_curnomeField;
    [OptionalField]
    private string HabilAnt_cursoField;
    [OptionalField]
    private string HabilAnt_estabField;
    [OptionalField]
    private string HabilAnt_estnomeField;
    [OptionalField]
    private string HabilAnt_grauField;
    [OptionalField]
    private string HabilAnt_graunomeField;
    [OptionalField]
    private string IDEstabField;
    [OptionalField]
    private string Ingresso_notaField;
    [OptionalField]
    private string Ingresso_opcaoField;
    [OptionalField]
    private string InscAnt_estabField;
    [OptionalField]
    private string InscAnt_estnomeField;
    [OptionalField]
    private string LocalidadeField;
    [OptionalField]
    private string NacionalidadeField;
    [OptionalField]
    private string Nacionalidade2Field;
    [OptionalField]
    private string NaturalidadeField;
    [OptionalField]
    private string NivelEscolar_MaeField;
    [OptionalField]
    private string NivelEscolar_PaiField;
    [OptionalField]
    private string NomeField;
    [OptionalField]
    private string NomeMaeField;
    [OptionalField]
    private string NomePaiField;
    [OptionalField]
    private string NumCandAntField;
    [OptionalField]
    private string NumIDField;
    [OptionalField]
    private string NumInscField;
    [OptionalField]
    private string NumInscAntField;
    [OptionalField]
    private string NumProcessoField;
    [OptionalField]
    private string ObservField;
    [OptionalField]
    private string PrimeiraVezField;
    [OptionalField]
    private string Profissao_AlunoField;
    [OptionalField]
    private string Profissao_MaeField;
    [OptionalField]
    private string Profissao_PaiField;
    [OptionalField]
    private string ProgComunitarioField;
    [OptionalField]
    private string RamoField;
    [OptionalField]
    private string RegimeFreqField;
    [OptionalField]
    private string ResidePaisField;
    [OptionalField]
    private string Retencao_PreUnivField;
    [OptionalField]
    private string SexoField;
    [OptionalField]
    private string SitProf_AlunoField;
    [OptionalField]
    private string SitProf_MaeField;
    [OptionalField]
    private string SitProf_PaiField;
    [OptionalField]
    private string TelefoneField;
    [OptionalField]
    private string TelemovelField;
    [OptionalField]
    private string TempoParcialField;
    [OptionalField]
    private string TipoEstabSecField;
    [OptionalField]
    private string TipoIDField;
    [OptionalField]
    private string biArquivoField;
    [OptionalField]
    private DateTime biDataEmissaoField;
    [OptionalField]
    private string codNaturalConcelhoField;
    [OptionalField]
    private string codNaturalDistritoField;
    [OptionalField]
    private string codNaturalFreguesiaField;
    [OptionalField]
    private string codRegimeField;
    [OptionalField]
    private string codResideConcelhoField;
    [OptionalField]
    private string codResideDistritoField;
    [OptionalField]
    private string nibField;
    [OptionalField]
    private string nifField;
    [OptionalField]
    private string nomeRegimeField;
    [OptionalField]
    private string repnifField;

    [Browsable(false)]
    public ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [DataMember]
    public string AltData
    {
      get
      {
        return this.AltDataField;
      }
      set
      {
        if ((object) this.AltDataField == (object) value)
          return;
        this.AltDataField = value;
        this.RaisePropertyChanged("AltData");
      }
    }

    [DataMember]
    public string AltUser
    {
      get
      {
        return this.AltUserField;
      }
      set
      {
        if ((object) this.AltUserField == (object) value)
          return;
        this.AltUserField = value;
        this.RaisePropertyChanged("AltUser");
      }
    }

    [DataMember]
    public string AlunoDeslocado
    {
      get
      {
        return this.AlunoDeslocadoField;
      }
      set
      {
        if ((object) this.AlunoDeslocadoField == (object) value)
          return;
        this.AlunoDeslocadoField = value;
        this.RaisePropertyChanged("AlunoDeslocado");
      }
    }

    [DataMember]
    public string Ano
    {
      get
      {
        return this.AnoField;
      }
      set
      {
        if ((object) this.AnoField == (object) value)
          return;
        this.AnoField = value;
        this.RaisePropertyChanged("Ano");
      }
    }

    [DataMember]
    public string AnoCurricular
    {
      get
      {
        return this.AnoCurricularField;
      }
      set
      {
        if ((object) this.AnoCurricularField == (object) value)
          return;
        this.AnoCurricularField = value;
        this.RaisePropertyChanged("AnoCurricular");
      }
    }

    [DataMember]
    public string Bolseiro
    {
      get
      {
        return this.BolseiroField;
      }
      set
      {
        if ((object) this.BolseiroField == (object) value)
          return;
        this.BolseiroField = value;
        this.RaisePropertyChanged("Bolseiro");
      }
    }

    [DataMember]
    public string Bolseiro_Inst
    {
      get
      {
        return this.Bolseiro_InstField;
      }
      set
      {
        if ((object) this.Bolseiro_InstField == (object) value)
          return;
        this.Bolseiro_InstField = value;
        this.RaisePropertyChanged("Bolseiro_Inst");
      }
    }

    [DataMember]
    public string Ciclo
    {
      get
      {
        return this.CicloField;
      }
      set
      {
        if ((object) this.CicloField == (object) value)
          return;
        this.CicloField = value;
        this.RaisePropertyChanged("Ciclo");
      }
    }

    [DataMember]
    public string CodPostal
    {
      get
      {
        return this.CodPostalField;
      }
      set
      {
        if ((object) this.CodPostalField == (object) value)
          return;
        this.CodPostalField = value;
        this.RaisePropertyChanged("CodPostal");
      }
    }

    [DataMember]
    public string Curso
    {
      get
      {
        return this.CursoField;
      }
      set
      {
        if ((object) this.CursoField == (object) value)
          return;
        this.CursoField = value;
        this.RaisePropertyChanged("Curso");
      }
    }

    [DataMember]
    public DateTime DataNasc
    {
      get
      {
        return this.DataNascField;
      }
      set
      {
        if (this.DataNascField.Equals(value))
          return;
        this.DataNascField = value;
        this.RaisePropertyChanged("DataNasc");
      }
    }

    [DataMember]
    public string EMail
    {
      get
      {
        return this.EMailField;
      }
      set
      {
        if ((object) this.EMailField == (object) value)
          return;
        this.EMailField = value;
        this.RaisePropertyChanged("EMail");
      }
    }

    [DataMember]
    public string Endereco
    {
      get
      {
        return this.EnderecoField;
      }
      set
      {
        if ((object) this.EnderecoField == (object) value)
          return;
        this.EnderecoField = value;
        this.RaisePropertyChanged("Endereco");
      }
    }

    [DataMember]
    public string Estab
    {
      get
      {
        return this.EstabField;
      }
      set
      {
        if ((object) this.EstabField == (object) value)
          return;
        this.EstabField = value;
        this.RaisePropertyChanged("Estab");
      }
    }

    [DataMember]
    public string EstadoCivil
    {
      get
      {
        return this.EstadoCivilField;
      }
      set
      {
        if ((object) this.EstadoCivilField == (object) value)
          return;
        this.EstadoCivilField = value;
        this.RaisePropertyChanged("EstadoCivil");
      }
    }

    [DataMember]
    public string EstudanteTrabalhador
    {
      get
      {
        return this.EstudanteTrabalhadorField;
      }
      set
      {
        if ((object) this.EstudanteTrabalhadorField == (object) value)
          return;
        this.EstudanteTrabalhadorField = value;
        this.RaisePropertyChanged("EstudanteTrabalhador");
      }
    }

    [DataMember]
    public string FormaIngresso
    {
      get
      {
        return this.FormaIngressoField;
      }
      set
      {
        if ((object) this.FormaIngressoField == (object) value)
          return;
        this.FormaIngressoField = value;
        this.RaisePropertyChanged("FormaIngresso");
      }
    }

    [DataMember]
    public string FreqOutros
    {
      get
      {
        return this.FreqOutrosField;
      }
      set
      {
        if ((object) this.FreqOutrosField == (object) value)
          return;
        this.FreqOutrosField = value;
        this.RaisePropertyChanged("FreqOutros");
      }
    }

    [DataMember]
    public string HabilAnt_AnoConc
    {
      get
      {
        return this.HabilAnt_AnoConcField;
      }
      set
      {
        if ((object) this.HabilAnt_AnoConcField == (object) value)
          return;
        this.HabilAnt_AnoConcField = value;
        this.RaisePropertyChanged("HabilAnt_AnoConc");
      }
    }

    [DataMember]
    public string HabilAnt_Pais
    {
      get
      {
        return this.HabilAnt_PaisField;
      }
      set
      {
        if ((object) this.HabilAnt_PaisField == (object) value)
          return;
        this.HabilAnt_PaisField = value;
        this.RaisePropertyChanged("HabilAnt_Pais");
      }
    }

    [DataMember]
    public string HabilAnt_curnome
    {
      get
      {
        return this.HabilAnt_curnomeField;
      }
      set
      {
        if ((object) this.HabilAnt_curnomeField == (object) value)
          return;
        this.HabilAnt_curnomeField = value;
        this.RaisePropertyChanged("HabilAnt_curnome");
      }
    }

    [DataMember]
    public string HabilAnt_curso
    {
      get
      {
        return this.HabilAnt_cursoField;
      }
      set
      {
        if ((object) this.HabilAnt_cursoField == (object) value)
          return;
        this.HabilAnt_cursoField = value;
        this.RaisePropertyChanged("HabilAnt_curso");
      }
    }

    [DataMember]
    public string HabilAnt_estab
    {
      get
      {
        return this.HabilAnt_estabField;
      }
      set
      {
        if ((object) this.HabilAnt_estabField == (object) value)
          return;
        this.HabilAnt_estabField = value;
        this.RaisePropertyChanged("HabilAnt_estab");
      }
    }

    [DataMember]
    public string HabilAnt_estnome
    {
      get
      {
        return this.HabilAnt_estnomeField;
      }
      set
      {
        if ((object) this.HabilAnt_estnomeField == (object) value)
          return;
        this.HabilAnt_estnomeField = value;
        this.RaisePropertyChanged("HabilAnt_estnome");
      }
    }

    [DataMember]
    public string HabilAnt_grau
    {
      get
      {
        return this.HabilAnt_grauField;
      }
      set
      {
        if ((object) this.HabilAnt_grauField == (object) value)
          return;
        this.HabilAnt_grauField = value;
        this.RaisePropertyChanged("HabilAnt_grau");
      }
    }

    [DataMember]
    public string HabilAnt_graunome
    {
      get
      {
        return this.HabilAnt_graunomeField;
      }
      set
      {
        if ((object) this.HabilAnt_graunomeField == (object) value)
          return;
        this.HabilAnt_graunomeField = value;
        this.RaisePropertyChanged("HabilAnt_graunome");
      }
    }

    [DataMember]
    public string IDEstab
    {
      get
      {
        return this.IDEstabField;
      }
      set
      {
        if ((object) this.IDEstabField == (object) value)
          return;
        this.IDEstabField = value;
        this.RaisePropertyChanged("IDEstab");
      }
    }

    [DataMember]
    public string Ingresso_nota
    {
      get
      {
        return this.Ingresso_notaField;
      }
      set
      {
        if ((object) this.Ingresso_notaField == (object) value)
          return;
        this.Ingresso_notaField = value;
        this.RaisePropertyChanged("Ingresso_nota");
      }
    }

    [DataMember]
    public string Ingresso_opcao
    {
      get
      {
        return this.Ingresso_opcaoField;
      }
      set
      {
        if ((object) this.Ingresso_opcaoField == (object) value)
          return;
        this.Ingresso_opcaoField = value;
        this.RaisePropertyChanged("Ingresso_opcao");
      }
    }

    [DataMember]
    public string InscAnt_estab
    {
      get
      {
        return this.InscAnt_estabField;
      }
      set
      {
        if ((object) this.InscAnt_estabField == (object) value)
          return;
        this.InscAnt_estabField = value;
        this.RaisePropertyChanged("InscAnt_estab");
      }
    }

    [DataMember]
    public string InscAnt_estnome
    {
      get
      {
        return this.InscAnt_estnomeField;
      }
      set
      {
        if ((object) this.InscAnt_estnomeField == (object) value)
          return;
        this.InscAnt_estnomeField = value;
        this.RaisePropertyChanged("InscAnt_estnome");
      }
    }

    [DataMember]
    public string Localidade
    {
      get
      {
        return this.LocalidadeField;
      }
      set
      {
        if ((object) this.LocalidadeField == (object) value)
          return;
        this.LocalidadeField = value;
        this.RaisePropertyChanged("Localidade");
      }
    }

    [DataMember]
    public string Nacionalidade
    {
      get
      {
        return this.NacionalidadeField;
      }
      set
      {
        if ((object) this.NacionalidadeField == (object) value)
          return;
        this.NacionalidadeField = value;
        this.RaisePropertyChanged("Nacionalidade");
      }
    }

    [DataMember]
    public string Nacionalidade2
    {
      get
      {
        return this.Nacionalidade2Field;
      }
      set
      {
        if ((object) this.Nacionalidade2Field == (object) value)
          return;
        this.Nacionalidade2Field = value;
        this.RaisePropertyChanged("Nacionalidade2");
      }
    }

    [DataMember]
    public string Naturalidade
    {
      get
      {
        return this.NaturalidadeField;
      }
      set
      {
        if ((object) this.NaturalidadeField == (object) value)
          return;
        this.NaturalidadeField = value;
        this.RaisePropertyChanged("Naturalidade");
      }
    }

    [DataMember]
    public string NivelEscolar_Mae
    {
      get
      {
        return this.NivelEscolar_MaeField;
      }
      set
      {
        if ((object) this.NivelEscolar_MaeField == (object) value)
          return;
        this.NivelEscolar_MaeField = value;
        this.RaisePropertyChanged("NivelEscolar_Mae");
      }
    }

    [DataMember]
    public string NivelEscolar_Pai
    {
      get
      {
        return this.NivelEscolar_PaiField;
      }
      set
      {
        if ((object) this.NivelEscolar_PaiField == (object) value)
          return;
        this.NivelEscolar_PaiField = value;
        this.RaisePropertyChanged("NivelEscolar_Pai");
      }
    }

    [DataMember]
    public string Nome
    {
      get
      {
        return this.NomeField;
      }
      set
      {
        if ((object) this.NomeField == (object) value)
          return;
        this.NomeField = value;
        this.RaisePropertyChanged("Nome");
      }
    }

    [DataMember]
    public string NomeMae
    {
      get
      {
        return this.NomeMaeField;
      }
      set
      {
        if ((object) this.NomeMaeField == (object) value)
          return;
        this.NomeMaeField = value;
        this.RaisePropertyChanged("NomeMae");
      }
    }

    [DataMember]
    public string NomePai
    {
      get
      {
        return this.NomePaiField;
      }
      set
      {
        if ((object) this.NomePaiField == (object) value)
          return;
        this.NomePaiField = value;
        this.RaisePropertyChanged("NomePai");
      }
    }

    [DataMember]
    public string NumCandAnt
    {
      get
      {
        return this.NumCandAntField;
      }
      set
      {
        if ((object) this.NumCandAntField == (object) value)
          return;
        this.NumCandAntField = value;
        this.RaisePropertyChanged("NumCandAnt");
      }
    }

    [DataMember]
    public string NumID
    {
      get
      {
        return this.NumIDField;
      }
      set
      {
        if ((object) this.NumIDField == (object) value)
          return;
        this.NumIDField = value;
        this.RaisePropertyChanged("NumID");
      }
    }

    [DataMember]
    public string NumInsc
    {
      get
      {
        return this.NumInscField;
      }
      set
      {
        if ((object) this.NumInscField == (object) value)
          return;
        this.NumInscField = value;
        this.RaisePropertyChanged("NumInsc");
      }
    }

    [DataMember]
    public string NumInscAnt
    {
      get
      {
        return this.NumInscAntField;
      }
      set
      {
        if ((object) this.NumInscAntField == (object) value)
          return;
        this.NumInscAntField = value;
        this.RaisePropertyChanged("NumInscAnt");
      }
    }

    [DataMember]
    public string NumProcesso
    {
      get
      {
        return this.NumProcessoField;
      }
      set
      {
        if ((object) this.NumProcessoField == (object) value)
          return;
        this.NumProcessoField = value;
        this.RaisePropertyChanged("NumProcesso");
      }
    }

    [DataMember]
    public string Observ
    {
      get
      {
        return this.ObservField;
      }
      set
      {
        if ((object) this.ObservField == (object) value)
          return;
        this.ObservField = value;
        this.RaisePropertyChanged("Observ");
      }
    }

    [DataMember]
    public string PrimeiraVez
    {
      get
      {
        return this.PrimeiraVezField;
      }
      set
      {
        if ((object) this.PrimeiraVezField == (object) value)
          return;
        this.PrimeiraVezField = value;
        this.RaisePropertyChanged("PrimeiraVez");
      }
    }

    [DataMember]
    public string Profissao_Aluno
    {
      get
      {
        return this.Profissao_AlunoField;
      }
      set
      {
        if ((object) this.Profissao_AlunoField == (object) value)
          return;
        this.Profissao_AlunoField = value;
        this.RaisePropertyChanged("Profissao_Aluno");
      }
    }

    [DataMember]
    public string Profissao_Mae
    {
      get
      {
        return this.Profissao_MaeField;
      }
      set
      {
        if ((object) this.Profissao_MaeField == (object) value)
          return;
        this.Profissao_MaeField = value;
        this.RaisePropertyChanged("Profissao_Mae");
      }
    }

    [DataMember]
    public string Profissao_Pai
    {
      get
      {
        return this.Profissao_PaiField;
      }
      set
      {
        if ((object) this.Profissao_PaiField == (object) value)
          return;
        this.Profissao_PaiField = value;
        this.RaisePropertyChanged("Profissao_Pai");
      }
    }

    [DataMember]
    public string ProgComunitario
    {
      get
      {
        return this.ProgComunitarioField;
      }
      set
      {
        if ((object) this.ProgComunitarioField == (object) value)
          return;
        this.ProgComunitarioField = value;
        this.RaisePropertyChanged("ProgComunitario");
      }
    }

    [DataMember]
    public string Ramo
    {
      get
      {
        return this.RamoField;
      }
      set
      {
        if ((object) this.RamoField == (object) value)
          return;
        this.RamoField = value;
        this.RaisePropertyChanged("Ramo");
      }
    }

    [DataMember]
    public string RegimeFreq
    {
      get
      {
        return this.RegimeFreqField;
      }
      set
      {
        if ((object) this.RegimeFreqField == (object) value)
          return;
        this.RegimeFreqField = value;
        this.RaisePropertyChanged("RegimeFreq");
      }
    }

    [DataMember]
    public string ResidePais
    {
      get
      {
        return this.ResidePaisField;
      }
      set
      {
        if ((object) this.ResidePaisField == (object) value)
          return;
        this.ResidePaisField = value;
        this.RaisePropertyChanged("ResidePais");
      }
    }

    [DataMember]
    public string Retencao_PreUniv
    {
      get
      {
        return this.Retencao_PreUnivField;
      }
      set
      {
        if ((object) this.Retencao_PreUnivField == (object) value)
          return;
        this.Retencao_PreUnivField = value;
        this.RaisePropertyChanged("Retencao_PreUniv");
      }
    }

    [DataMember]
    public string Sexo
    {
      get
      {
        return this.SexoField;
      }
      set
      {
        if ((object) this.SexoField == (object) value)
          return;
        this.SexoField = value;
        this.RaisePropertyChanged("Sexo");
      }
    }

    [DataMember]
    public string SitProf_Aluno
    {
      get
      {
        return this.SitProf_AlunoField;
      }
      set
      {
        if ((object) this.SitProf_AlunoField == (object) value)
          return;
        this.SitProf_AlunoField = value;
        this.RaisePropertyChanged("SitProf_Aluno");
      }
    }

    [DataMember]
    public string SitProf_Mae
    {
      get
      {
        return this.SitProf_MaeField;
      }
      set
      {
        if ((object) this.SitProf_MaeField == (object) value)
          return;
        this.SitProf_MaeField = value;
        this.RaisePropertyChanged("SitProf_Mae");
      }
    }

    [DataMember]
    public string SitProf_Pai
    {
      get
      {
        return this.SitProf_PaiField;
      }
      set
      {
        if ((object) this.SitProf_PaiField == (object) value)
          return;
        this.SitProf_PaiField = value;
        this.RaisePropertyChanged("SitProf_Pai");
      }
    }

    [DataMember]
    public string Telefone
    {
      get
      {
        return this.TelefoneField;
      }
      set
      {
        if ((object) this.TelefoneField == (object) value)
          return;
        this.TelefoneField = value;
        this.RaisePropertyChanged("Telefone");
      }
    }

    [DataMember]
    public string Telemovel
    {
      get
      {
        return this.TelemovelField;
      }
      set
      {
        if ((object) this.TelemovelField == (object) value)
          return;
        this.TelemovelField = value;
        this.RaisePropertyChanged("Telemovel");
      }
    }

    [DataMember]
    public string TempoParcial
    {
      get
      {
        return this.TempoParcialField;
      }
      set
      {
        if ((object) this.TempoParcialField == (object) value)
          return;
        this.TempoParcialField = value;
        this.RaisePropertyChanged("TempoParcial");
      }
    }

    [DataMember]
    public string TipoEstabSec
    {
      get
      {
        return this.TipoEstabSecField;
      }
      set
      {
        if ((object) this.TipoEstabSecField == (object) value)
          return;
        this.TipoEstabSecField = value;
        this.RaisePropertyChanged("TipoEstabSec");
      }
    }

    [DataMember]
    public string TipoID
    {
      get
      {
        return this.TipoIDField;
      }
      set
      {
        if ((object) this.TipoIDField == (object) value)
          return;
        this.TipoIDField = value;
        this.RaisePropertyChanged("TipoID");
      }
    }

    [DataMember]
    public string biArquivo
    {
      get
      {
        return this.biArquivoField;
      }
      set
      {
        if ((object) this.biArquivoField == (object) value)
          return;
        this.biArquivoField = value;
        this.RaisePropertyChanged("biArquivo");
      }
    }

    [DataMember]
    public DateTime biDataEmissao
    {
      get
      {
        return this.biDataEmissaoField;
      }
      set
      {
        if (this.biDataEmissaoField.Equals(value))
          return;
        this.biDataEmissaoField = value;
        this.RaisePropertyChanged("biDataEmissao");
      }
    }

    [DataMember]
    public string codNaturalConcelho
    {
      get
      {
        return this.codNaturalConcelhoField;
      }
      set
      {
        if ((object) this.codNaturalConcelhoField == (object) value)
          return;
        this.codNaturalConcelhoField = value;
        this.RaisePropertyChanged("codNaturalConcelho");
      }
    }

    [DataMember]
    public string codNaturalDistrito
    {
      get
      {
        return this.codNaturalDistritoField;
      }
      set
      {
        if ((object) this.codNaturalDistritoField == (object) value)
          return;
        this.codNaturalDistritoField = value;
        this.RaisePropertyChanged("codNaturalDistrito");
      }
    }

    [DataMember]
    public string codNaturalFreguesia
    {
      get
      {
        return this.codNaturalFreguesiaField;
      }
      set
      {
        if ((object) this.codNaturalFreguesiaField == (object) value)
          return;
        this.codNaturalFreguesiaField = value;
        this.RaisePropertyChanged("codNaturalFreguesia");
      }
    }

    [DataMember]
    public string codRegime
    {
      get
      {
        return this.codRegimeField;
      }
      set
      {
        if ((object) this.codRegimeField == (object) value)
          return;
        this.codRegimeField = value;
        this.RaisePropertyChanged("codRegime");
      }
    }

    [DataMember]
    public string codResideConcelho
    {
      get
      {
        return this.codResideConcelhoField;
      }
      set
      {
        if ((object) this.codResideConcelhoField == (object) value)
          return;
        this.codResideConcelhoField = value;
        this.RaisePropertyChanged("codResideConcelho");
      }
    }

    [DataMember]
    public string codResideDistrito
    {
      get
      {
        return this.codResideDistritoField;
      }
      set
      {
        if ((object) this.codResideDistritoField == (object) value)
          return;
        this.codResideDistritoField = value;
        this.RaisePropertyChanged("codResideDistrito");
      }
    }

    [DataMember]
    public string nib
    {
      get
      {
        return this.nibField;
      }
      set
      {
        if ((object) this.nibField == (object) value)
          return;
        this.nibField = value;
        this.RaisePropertyChanged("nib");
      }
    }

    [DataMember]
    public string nif
    {
      get
      {
        return this.nifField;
      }
      set
      {
        if ((object) this.nifField == (object) value)
          return;
        this.nifField = value;
        this.RaisePropertyChanged("nif");
      }
    }

    [DataMember]
    public string nomeRegime
    {
      get
      {
        return this.nomeRegimeField;
      }
      set
      {
        if ((object) this.nomeRegimeField == (object) value)
          return;
        this.nomeRegimeField = value;
        this.RaisePropertyChanged("nomeRegime");
      }
    }

    [DataMember]
    public string repnif
    {
      get
      {
        return this.repnifField;
      }
      set
      {
        if ((object) this.repnifField == (object) value)
          return;
        this.repnifField = value;
        this.RaisePropertyChanged("repnif");
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
