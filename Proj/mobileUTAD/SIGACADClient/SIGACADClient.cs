﻿// Decompiled with JetBrains decompiler
// Type: mobileUTAD.SIGACADClient.SIGACADClient
// Assembly: mobileUTAD, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C6335312-308C-477B-9BDC-E68404688049
// Assembly location: A:\Git repositories\mobileUTAD\bin\mobileUTAD.dll

using System.CodeDom.Compiler;
using System.Data;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace mobileUTAD.SIGACADClient
{
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  public class SIGACADClient : ClientBase<ISIGACAD>, ISIGACAD
  {
    public SIGACADClient()
    {
    }

    public SIGACADClient(string endpointConfigurationName)
      : base(endpointConfigurationName)
    {
    }

    public SIGACADClient(string endpointConfigurationName, string remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public SIGACADClient(string endpointConfigurationName, EndpointAddress remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public SIGACADClient(Binding binding, EndpointAddress remoteAddress)
      : base(binding, remoteAddress)
    {
    }

    public void DoWork()
    {
      this.Channel.DoWork();
    }

    public DataTable lerRegime(string numMec)
    {
      return this.Channel.lerRegime(numMec);
    }

    public Regime[] GetAlunoRegime(string numMec)
    {
      return this.Channel.GetAlunoRegime(numMec);
    }

    public alunoPlanoCurricular[] GetAlunoNotas(string numMec)
    {
      return this.Channel.GetAlunoNotas(numMec);
    }

    public alunoInscricoes[] GetAlunoInscricoes(string numMec)
    {
      return this.Channel.GetAlunoInscricoes(numMec);
    }

    public alunoPlanoCurricular[] GetAlunoPlanoCurricular(string numMec)
    {
      return this.Channel.GetAlunoPlanoCurricular(numMec);
    }

    public AlunoSIGACAD[] GetAlunoDadosPessoais(string numMec)
    {
      return this.Channel.GetAlunoDadosPessoais(numMec);
    }

    public propinasAluno[] GetAlunoPropinas(string numMec)
    {
      return this.Channel.GetAlunoPropinas(numMec);
    }

    public dividasAluno[] GetAlunoDividas(string numMec)
    {
      return this.Channel.GetAlunoDividas(numMec);
    }

    public propinasAluno[] GetAlunoPropinasPagas(string numMec)
    {
      return this.Channel.GetAlunoPropinasPagas(numMec);
    }

    public alunoPlanoCurricular[] GetAlunoUCEmFalta(string numMec)
    {
      return this.Channel.GetAlunoUCEmFalta(numMec);
    }

    public NomeAlunoCursoAluno[] GetNomeAlunoCursoAluno(string numMec)
    {
      return this.Channel.GetNomeAlunoCursoAluno(numMec);
    }

    public alunoCursos[] GetAlunoCursos(string numMec)
    {
      return this.Channel.GetAlunoCursos(numMec);
    }
  }
}
