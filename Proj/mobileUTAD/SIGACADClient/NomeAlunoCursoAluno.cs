﻿// Decompiled with JetBrains decompiler
// Type: mobileUTAD.SIGACADClient.NomeAlunoCursoAluno
// Assembly: mobileUTAD, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C6335312-308C-477B-9BDC-E68404688049
// Assembly location: A:\Git repositories\mobileUTAD\bin\mobileUTAD.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace mobileUTAD.SIGACADClient
{
  [DebuggerStepThrough]
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DataContract(Name = "NomeAlunoCursoAluno", Namespace = "http://schemas.datacontract.org/2004/07/wsSIGACAD2")]
  [Serializable]
  public class NomeAlunoCursoAluno : IExtensibleDataObject, INotifyPropertyChanged
  {
    [NonSerialized]
    private ExtensionDataObject extensionDataField;
    [OptionalField]
    private string anoCurricularField;
    [OptionalField]
    private string anoInscricaoField;
    [OptionalField]
    private string codCursoField;
    [OptionalField]
    private string cursoField;
    [OptionalField]
    private string datainscricaoField;
    [OptionalField]
    private string nomeField;
    [OptionalField]
    private int numeroField;
    [OptionalField]
    private string siglaCursoField;

    [Browsable(false)]
    public ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [DataMember]
    public string anoCurricular
    {
      get
      {
        return this.anoCurricularField;
      }
      set
      {
        if ((object) this.anoCurricularField == (object) value)
          return;
        this.anoCurricularField = value;
        this.RaisePropertyChanged("anoCurricular");
      }
    }

    [DataMember]
    public string anoInscricao
    {
      get
      {
        return this.anoInscricaoField;
      }
      set
      {
        if ((object) this.anoInscricaoField == (object) value)
          return;
        this.anoInscricaoField = value;
        this.RaisePropertyChanged("anoInscricao");
      }
    }

    [DataMember]
    public string codCurso
    {
      get
      {
        return this.codCursoField;
      }
      set
      {
        if ((object) this.codCursoField == (object) value)
          return;
        this.codCursoField = value;
        this.RaisePropertyChanged("codCurso");
      }
    }

    [DataMember]
    public string curso
    {
      get
      {
        return this.cursoField;
      }
      set
      {
        if ((object) this.cursoField == (object) value)
          return;
        this.cursoField = value;
        this.RaisePropertyChanged("curso");
      }
    }

    [DataMember]
    public string datainscricao
    {
      get
      {
        return this.datainscricaoField;
      }
      set
      {
        if ((object) this.datainscricaoField == (object) value)
          return;
        this.datainscricaoField = value;
        this.RaisePropertyChanged("datainscricao");
      }
    }

    [DataMember]
    public string nome
    {
      get
      {
        return this.nomeField;
      }
      set
      {
        if ((object) this.nomeField == (object) value)
          return;
        this.nomeField = value;
        this.RaisePropertyChanged("nome");
      }
    }

    [DataMember]
    public int numero
    {
      get
      {
        return this.numeroField;
      }
      set
      {
        if (this.numeroField.Equals(value))
          return;
        this.numeroField = value;
        this.RaisePropertyChanged("numero");
      }
    }

    [DataMember]
    public string siglaCurso
    {
      get
      {
        return this.siglaCursoField;
      }
      set
      {
        if ((object) this.siglaCursoField == (object) value)
          return;
        this.siglaCursoField = value;
        this.RaisePropertyChanged("siglaCurso");
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
