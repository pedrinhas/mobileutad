﻿// Decompiled with JetBrains decompiler
// Type: mobileUTAD.SIGACADClient.alunoPlanoCurricular
// Assembly: mobileUTAD, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C6335312-308C-477B-9BDC-E68404688049
// Assembly location: A:\Git repositories\mobileUTAD\bin\mobileUTAD.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace mobileUTAD.SIGACADClient
{
  [DebuggerStepThrough]
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DataContract(Name = "alunoPlanoCurricular", Namespace = "http://schemas.datacontract.org/2004/07/wsSIGACAD2")]
  [Serializable]
  public class alunoPlanoCurricular : IExtensibleDataObject, INotifyPropertyChanged
  {
    [NonSerialized]
    private ExtensionDataObject extensionDataField;
    [OptionalField]
    private int IDMatriculaField;
    [OptionalField]
    private int IDRamoField;
    [OptionalField]
    private string IPField;
    [OptionalField]
    private int RefidOrdemField;
    [OptionalField]
    private int TipoDiscipField;
    [OptionalField]
    private int anoField;
    [OptionalField]
    private int codCursoField;
    [OptionalField]
    private int codDisciplinaField;
    [OptionalField]
    private double creditosField;
    [OptionalField]
    private string cursoField;
    [OptionalField]
    private string disciplinaField;
    [OptionalField]
    private double ectsField;
    [OptionalField]
    private int epocaexameField;
    [OptionalField]
    private mobileUTAD.SIGACADClient.grupoOpcional[] grupoOpcionalField;
    [OptionalField]
    private int inscritoField;
    [OptionalField]
    private string notaField;
    [OptionalField]
    private int primeira_vezField;
    [OptionalField]
    private int semestreField;
    [OptionalField]
    private int transferidoField;

    [Browsable(false)]
    public ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [DataMember]
    public int IDMatricula
    {
      get
      {
        return this.IDMatriculaField;
      }
      set
      {
        if (this.IDMatriculaField.Equals(value))
          return;
        this.IDMatriculaField = value;
        this.RaisePropertyChanged("IDMatricula");
      }
    }

    [DataMember]
    public int IDRamo
    {
      get
      {
        return this.IDRamoField;
      }
      set
      {
        if (this.IDRamoField.Equals(value))
          return;
        this.IDRamoField = value;
        this.RaisePropertyChanged("IDRamo");
      }
    }

    [DataMember]
    public string IP
    {
      get
      {
        return this.IPField;
      }
      set
      {
        if ((object) this.IPField == (object) value)
          return;
        this.IPField = value;
        this.RaisePropertyChanged("IP");
      }
    }

    [DataMember]
    public int RefidOrdem
    {
      get
      {
        return this.RefidOrdemField;
      }
      set
      {
        if (this.RefidOrdemField.Equals(value))
          return;
        this.RefidOrdemField = value;
        this.RaisePropertyChanged("RefidOrdem");
      }
    }

    [DataMember]
    public int TipoDiscip
    {
      get
      {
        return this.TipoDiscipField;
      }
      set
      {
        if (this.TipoDiscipField.Equals(value))
          return;
        this.TipoDiscipField = value;
        this.RaisePropertyChanged("TipoDiscip");
      }
    }

    [DataMember]
    public int ano
    {
      get
      {
        return this.anoField;
      }
      set
      {
        if (this.anoField.Equals(value))
          return;
        this.anoField = value;
        this.RaisePropertyChanged("ano");
      }
    }

    [DataMember]
    public int codCurso
    {
      get
      {
        return this.codCursoField;
      }
      set
      {
        if (this.codCursoField.Equals(value))
          return;
        this.codCursoField = value;
        this.RaisePropertyChanged("codCurso");
      }
    }

    [DataMember]
    public int codDisciplina
    {
      get
      {
        return this.codDisciplinaField;
      }
      set
      {
        if (this.codDisciplinaField.Equals(value))
          return;
        this.codDisciplinaField = value;
        this.RaisePropertyChanged("codDisciplina");
      }
    }

    [DataMember]
    public double creditos
    {
      get
      {
        return this.creditosField;
      }
      set
      {
        if (this.creditosField.Equals(value))
          return;
        this.creditosField = value;
        this.RaisePropertyChanged("creditos");
      }
    }

    [DataMember]
    public string curso
    {
      get
      {
        return this.cursoField;
      }
      set
      {
        if ((object) this.cursoField == (object) value)
          return;
        this.cursoField = value;
        this.RaisePropertyChanged("curso");
      }
    }

    [DataMember]
    public string disciplina
    {
      get
      {
        return this.disciplinaField;
      }
      set
      {
        if ((object) this.disciplinaField == (object) value)
          return;
        this.disciplinaField = value;
        this.RaisePropertyChanged("disciplina");
      }
    }

    [DataMember]
    public double ects
    {
      get
      {
        return this.ectsField;
      }
      set
      {
        if (this.ectsField.Equals(value))
          return;
        this.ectsField = value;
        this.RaisePropertyChanged("ects");
      }
    }

    [DataMember]
    public int epocaexame
    {
      get
      {
        return this.epocaexameField;
      }
      set
      {
        if (this.epocaexameField.Equals(value))
          return;
        this.epocaexameField = value;
        this.RaisePropertyChanged("epocaexame");
      }
    }

    [DataMember]
    public mobileUTAD.SIGACADClient.grupoOpcional[] grupoOpcional
    {
      get
      {
        return this.grupoOpcionalField;
      }
      set
      {
        if (this.grupoOpcionalField == value)
          return;
        this.grupoOpcionalField = value;
        this.RaisePropertyChanged("grupoOpcional");
      }
    }

    [DataMember]
    public int inscrito
    {
      get
      {
        return this.inscritoField;
      }
      set
      {
        if (this.inscritoField.Equals(value))
          return;
        this.inscritoField = value;
        this.RaisePropertyChanged("inscrito");
      }
    }

    [DataMember]
    public string nota
    {
      get
      {
        return this.notaField;
      }
      set
      {
        if ((object) this.notaField == (object) value)
          return;
        this.notaField = value;
        this.RaisePropertyChanged("nota");
      }
    }

    [DataMember]
    public int primeira_vez
    {
      get
      {
        return this.primeira_vezField;
      }
      set
      {
        if (this.primeira_vezField.Equals(value))
          return;
        this.primeira_vezField = value;
        this.RaisePropertyChanged("primeira_vez");
      }
    }

    [DataMember]
    public int semestre
    {
      get
      {
        return this.semestreField;
      }
      set
      {
        if (this.semestreField.Equals(value))
          return;
        this.semestreField = value;
        this.RaisePropertyChanged("semestre");
      }
    }

    [DataMember]
    public int transferido
    {
      get
      {
        return this.transferidoField;
      }
      set
      {
        if (this.transferidoField.Equals(value))
          return;
        this.transferidoField = value;
        this.RaisePropertyChanged("transferido");
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
