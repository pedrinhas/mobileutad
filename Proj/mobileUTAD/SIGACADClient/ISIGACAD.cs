﻿// Decompiled with JetBrains decompiler
// Type: mobileUTAD.SIGACADClient.ISIGACAD
// Assembly: mobileUTAD, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C6335312-308C-477B-9BDC-E68404688049
// Assembly location: A:\Git repositories\mobileUTAD\bin\mobileUTAD.dll

using System.CodeDom.Compiler;
using System.Data;
using System.ServiceModel;

namespace mobileUTAD.SIGACADClient
{
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  [ServiceContract(ConfigurationName = "SIGACADClient.ISIGACAD")]
  public interface ISIGACAD
  {
    [OperationContract(Action = "http://tempuri.org/ISIGACAD/DoWork", ReplyAction = "http://tempuri.org/ISIGACAD/DoWorkResponse")]
    void DoWork();

    [OperationContract(Action = "http://tempuri.org/ISIGACAD/lerRegime", ReplyAction = "http://tempuri.org/ISIGACAD/lerRegimeResponse")]
    DataTable lerRegime(string numMec);

    [OperationContract(Action = "http://tempuri.org/ISIGACAD/GetAlunoRegime", ReplyAction = "http://tempuri.org/ISIGACAD/GetAlunoRegimeResponse")]
    Regime[] GetAlunoRegime(string numMec);

    [OperationContract(Action = "http://tempuri.org/ISIGACAD/GetAlunoNotas", ReplyAction = "http://tempuri.org/ISIGACAD/GetAlunoNotasResponse")]
    alunoPlanoCurricular[] GetAlunoNotas(string numMec);

    [OperationContract(Action = "http://tempuri.org/ISIGACAD/GetAlunoInscricoes", ReplyAction = "http://tempuri.org/ISIGACAD/GetAlunoInscricoesResponse")]
    alunoInscricoes[] GetAlunoInscricoes(string numMec);

    [OperationContract(Action = "http://tempuri.org/ISIGACAD/GetAlunoPlanoCurricular", ReplyAction = "http://tempuri.org/ISIGACAD/GetAlunoPlanoCurricularResponse")]
    alunoPlanoCurricular[] GetAlunoPlanoCurricular(string numMec);

    [OperationContract(Action = "http://tempuri.org/ISIGACAD/GetAlunoDadosPessoais", ReplyAction = "http://tempuri.org/ISIGACAD/GetAlunoDadosPessoaisResponse")]
    AlunoSIGACAD[] GetAlunoDadosPessoais(string numMec);

    [OperationContract(Action = "http://tempuri.org/ISIGACAD/GetAlunoPropinas", ReplyAction = "http://tempuri.org/ISIGACAD/GetAlunoPropinasResponse")]
    propinasAluno[] GetAlunoPropinas(string numMec);

    [OperationContract(Action = "http://tempuri.org/ISIGACAD/GetAlunoDividas", ReplyAction = "http://tempuri.org/ISIGACAD/GetAlunoDividasResponse")]
    dividasAluno[] GetAlunoDividas(string numMec);

    [OperationContract(Action = "http://tempuri.org/ISIGACAD/GetAlunoPropinasPagas", ReplyAction = "http://tempuri.org/ISIGACAD/GetAlunoPropinasPagasResponse")]
    propinasAluno[] GetAlunoPropinasPagas(string numMec);

    [OperationContract(Action = "http://tempuri.org/ISIGACAD/GetAlunoUCEmFalta", ReplyAction = "http://tempuri.org/ISIGACAD/GetAlunoUCEmFaltaResponse")]
    alunoPlanoCurricular[] GetAlunoUCEmFalta(string numMec);

    [OperationContract(Action = "http://tempuri.org/ISIGACAD/GetNomeAlunoCursoAluno", ReplyAction = "http://tempuri.org/ISIGACAD/GetNomeAlunoCursoAlunoResponse")]
    NomeAlunoCursoAluno[] GetNomeAlunoCursoAluno(string numMec);

    [OperationContract(Action = "http://tempuri.org/ISIGACAD/GetAlunoCursos", ReplyAction = "http://tempuri.org/ISIGACAD/GetAlunoCursosResponse")]
    alunoCursos[] GetAlunoCursos(string numMec);
  }
}
