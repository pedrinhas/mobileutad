﻿// Decompiled with JetBrains decompiler
// Type: mobileUTAD.SIGACADClient.alunoInscricoes
// Assembly: mobileUTAD, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C6335312-308C-477B-9BDC-E68404688049
// Assembly location: A:\Git repositories\mobileUTAD\bin\mobileUTAD.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace mobileUTAD.SIGACADClient
{
  [DebuggerStepThrough]
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DataContract(Name = "alunoInscricoes", Namespace = "http://schemas.datacontract.org/2004/07/wsSIGACAD2")]
  [Serializable]
  public class alunoInscricoes : IExtensibleDataObject, INotifyPropertyChanged
  {
    [NonSerialized]
    private ExtensionDataObject extensionDataField;
    [OptionalField]
    private int IDMatriculaField;
    [OptionalField]
    private string TipoExameField;
    [OptionalField]
    private int anoCurricularField;
    [OptionalField]
    private int anoLectivoField;
    [OptionalField]
    private int codCursoField;
    [OptionalField]
    private int codDisciplinaField;
    [OptionalField]
    private double creditosField;
    [OptionalField]
    private string cursoField;
    [OptionalField]
    private string dataInscricaoField;
    [OptionalField]
    private string departamentoField;
    [OptionalField]
    private double ectsField;
    [OptionalField]
    private int epocaexameField;
    [OptionalField]
    private string nomedisciplinaField;
    [OptionalField]
    private int primeira_vezField;
    [OptionalField]
    private int regAlunoField;
    [OptionalField]
    private int semestreField;
    [OptionalField]
    private int validadeField;

    [Browsable(false)]
    public ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [DataMember]
    public int IDMatricula
    {
      get
      {
        return this.IDMatriculaField;
      }
      set
      {
        if (this.IDMatriculaField.Equals(value))
          return;
        this.IDMatriculaField = value;
        this.RaisePropertyChanged("IDMatricula");
      }
    }

    [DataMember]
    public string TipoExame
    {
      get
      {
        return this.TipoExameField;
      }
      set
      {
        if ((object) this.TipoExameField == (object) value)
          return;
        this.TipoExameField = value;
        this.RaisePropertyChanged("TipoExame");
      }
    }

    [DataMember]
    public int anoCurricular
    {
      get
      {
        return this.anoCurricularField;
      }
      set
      {
        if (this.anoCurricularField.Equals(value))
          return;
        this.anoCurricularField = value;
        this.RaisePropertyChanged("anoCurricular");
      }
    }

    [DataMember]
    public int anoLectivo
    {
      get
      {
        return this.anoLectivoField;
      }
      set
      {
        if (this.anoLectivoField.Equals(value))
          return;
        this.anoLectivoField = value;
        this.RaisePropertyChanged("anoLectivo");
      }
    }

    [DataMember]
    public int codCurso
    {
      get
      {
        return this.codCursoField;
      }
      set
      {
        if (this.codCursoField.Equals(value))
          return;
        this.codCursoField = value;
        this.RaisePropertyChanged("codCurso");
      }
    }

    [DataMember]
    public int codDisciplina
    {
      get
      {
        return this.codDisciplinaField;
      }
      set
      {
        if (this.codDisciplinaField.Equals(value))
          return;
        this.codDisciplinaField = value;
        this.RaisePropertyChanged("codDisciplina");
      }
    }

    [DataMember]
    public double creditos
    {
      get
      {
        return this.creditosField;
      }
      set
      {
        if (this.creditosField.Equals(value))
          return;
        this.creditosField = value;
        this.RaisePropertyChanged("creditos");
      }
    }

    [DataMember]
    public string curso
    {
      get
      {
        return this.cursoField;
      }
      set
      {
        if ((object) this.cursoField == (object) value)
          return;
        this.cursoField = value;
        this.RaisePropertyChanged("curso");
      }
    }

    [DataMember]
    public string dataInscricao
    {
      get
      {
        return this.dataInscricaoField;
      }
      set
      {
        if ((object) this.dataInscricaoField == (object) value)
          return;
        this.dataInscricaoField = value;
        this.RaisePropertyChanged("dataInscricao");
      }
    }

    [DataMember]
    public string departamento
    {
      get
      {
        return this.departamentoField;
      }
      set
      {
        if ((object) this.departamentoField == (object) value)
          return;
        this.departamentoField = value;
        this.RaisePropertyChanged("departamento");
      }
    }

    [DataMember]
    public double ects
    {
      get
      {
        return this.ectsField;
      }
      set
      {
        if (this.ectsField.Equals(value))
          return;
        this.ectsField = value;
        this.RaisePropertyChanged("ects");
      }
    }

    [DataMember]
    public int epocaexame
    {
      get
      {
        return this.epocaexameField;
      }
      set
      {
        if (this.epocaexameField.Equals(value))
          return;
        this.epocaexameField = value;
        this.RaisePropertyChanged("epocaexame");
      }
    }

    [DataMember]
    public string nomedisciplina
    {
      get
      {
        return this.nomedisciplinaField;
      }
      set
      {
        if ((object) this.nomedisciplinaField == (object) value)
          return;
        this.nomedisciplinaField = value;
        this.RaisePropertyChanged("nomedisciplina");
      }
    }

    [DataMember]
    public int primeira_vez
    {
      get
      {
        return this.primeira_vezField;
      }
      set
      {
        if (this.primeira_vezField.Equals(value))
          return;
        this.primeira_vezField = value;
        this.RaisePropertyChanged("primeira_vez");
      }
    }

    [DataMember]
    public int regAluno
    {
      get
      {
        return this.regAlunoField;
      }
      set
      {
        if (this.regAlunoField.Equals(value))
          return;
        this.regAlunoField = value;
        this.RaisePropertyChanged("regAluno");
      }
    }

    [DataMember]
    public int semestre
    {
      get
      {
        return this.semestreField;
      }
      set
      {
        if (this.semestreField.Equals(value))
          return;
        this.semestreField = value;
        this.RaisePropertyChanged("semestre");
      }
    }

    [DataMember]
    public int validade
    {
      get
      {
        return this.validadeField;
      }
      set
      {
        if (this.validadeField.Equals(value))
          return;
        this.validadeField = value;
        this.RaisePropertyChanged("validade");
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
