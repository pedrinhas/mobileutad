﻿// Decompiled with JetBrains decompiler
// Type: mobileUTAD.SIGACADClient.dividasAluno
// Assembly: mobileUTAD, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C6335312-308C-477B-9BDC-E68404688049
// Assembly location: A:\Git repositories\mobileUTAD\bin\mobileUTAD.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace mobileUTAD.SIGACADClient
{
  [DebuggerStepThrough]
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DataContract(Name = "dividasAluno", Namespace = "http://schemas.datacontract.org/2004/07/wsSIGACAD2")]
  [Serializable]
  public class dividasAluno : IExtensibleDataObject, INotifyPropertyChanged
  {
    [NonSerialized]
    private ExtensionDataObject extensionDataField;
    [OptionalField]
    private string anoLectivoField;
    [OptionalField]
    private string datadividaField;
    [OptionalField]
    private string datalimiteField;
    [OptionalField]
    private string descritivoField;
    [OptionalField]
    private string nometipodividaField;
    [OptionalField]
    private string valordividaField;

    [Browsable(false)]
    public ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [DataMember]
    public string anoLectivo
    {
      get
      {
        return this.anoLectivoField;
      }
      set
      {
        if ((object) this.anoLectivoField == (object) value)
          return;
        this.anoLectivoField = value;
        this.RaisePropertyChanged("anoLectivo");
      }
    }

    [DataMember]
    public string datadivida
    {
      get
      {
        return this.datadividaField;
      }
      set
      {
        if ((object) this.datadividaField == (object) value)
          return;
        this.datadividaField = value;
        this.RaisePropertyChanged("datadivida");
      }
    }

    [DataMember]
    public string datalimite
    {
      get
      {
        return this.datalimiteField;
      }
      set
      {
        if ((object) this.datalimiteField == (object) value)
          return;
        this.datalimiteField = value;
        this.RaisePropertyChanged("datalimite");
      }
    }

    [DataMember]
    public string descritivo
    {
      get
      {
        return this.descritivoField;
      }
      set
      {
        if ((object) this.descritivoField == (object) value)
          return;
        this.descritivoField = value;
        this.RaisePropertyChanged("descritivo");
      }
    }

    [DataMember]
    public string nometipodivida
    {
      get
      {
        return this.nometipodividaField;
      }
      set
      {
        if ((object) this.nometipodividaField == (object) value)
          return;
        this.nometipodividaField = value;
        this.RaisePropertyChanged("nometipodivida");
      }
    }

    [DataMember]
    public string valordivida
    {
      get
      {
        return this.valordividaField;
      }
      set
      {
        if ((object) this.valordividaField == (object) value)
          return;
        this.valordividaField = value;
        this.RaisePropertyChanged("valordivida");
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
