﻿// Decompiled with JetBrains decompiler
// Type: mobileUTAD.SIGACADClient.alunoCursos
// Assembly: mobileUTAD, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C6335312-308C-477B-9BDC-E68404688049
// Assembly location: A:\Git repositories\mobileUTAD\bin\mobileUTAD.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace mobileUTAD.SIGACADClient
{
  [DebuggerStepThrough]
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DataContract(Name = "alunoCursos", Namespace = "http://schemas.datacontract.org/2004/07/wsSIGACAD2")]
  [Serializable]
  public class alunoCursos : IExtensibleDataObject, INotifyPropertyChanged
  {
    [NonSerialized]
    private ExtensionDataObject extensionDataField;
    [OptionalField]
    private string codCursoField;
    [OptionalField]
    private string codPlanoField;
    [OptionalField]
    private string idMatriculaField;
    [OptionalField]
    private string idRamoField;
    [OptionalField]
    private string nomeCursoField;
    [OptionalField]
    private string siglaCursoField;

    [Browsable(false)]
    public ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [DataMember]
    public string codCurso
    {
      get
      {
        return this.codCursoField;
      }
      set
      {
        if ((object) this.codCursoField == (object) value)
          return;
        this.codCursoField = value;
        this.RaisePropertyChanged("codCurso");
      }
    }

    [DataMember]
    public string codPlano
    {
      get
      {
        return this.codPlanoField;
      }
      set
      {
        if ((object) this.codPlanoField == (object) value)
          return;
        this.codPlanoField = value;
        this.RaisePropertyChanged("codPlano");
      }
    }

    [DataMember]
    public string idMatricula
    {
      get
      {
        return this.idMatriculaField;
      }
      set
      {
        if ((object) this.idMatriculaField == (object) value)
          return;
        this.idMatriculaField = value;
        this.RaisePropertyChanged("idMatricula");
      }
    }

    [DataMember]
    public string idRamo
    {
      get
      {
        return this.idRamoField;
      }
      set
      {
        if ((object) this.idRamoField == (object) value)
          return;
        this.idRamoField = value;
        this.RaisePropertyChanged("idRamo");
      }
    }

    [DataMember]
    public string nomeCurso
    {
      get
      {
        return this.nomeCursoField;
      }
      set
      {
        if ((object) this.nomeCursoField == (object) value)
          return;
        this.nomeCursoField = value;
        this.RaisePropertyChanged("nomeCurso");
      }
    }

    [DataMember]
    public string siglaCurso
    {
      get
      {
        return this.siglaCursoField;
      }
      set
      {
        if ((object) this.siglaCursoField == (object) value)
          return;
        this.siglaCursoField = value;
        this.RaisePropertyChanged("siglaCurso");
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
