﻿// Decompiled with JetBrains decompiler
// Type: mobileUTAD.SIGACADClient.grupoOpcional
// Assembly: mobileUTAD, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C6335312-308C-477B-9BDC-E68404688049
// Assembly location: A:\Git repositories\mobileUTAD\bin\mobileUTAD.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace mobileUTAD.SIGACADClient
{
  [DebuggerStepThrough]
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DataContract(Name = "grupoOpcional", Namespace = "http://schemas.datacontract.org/2004/07/wsSIGACAD2")]
  [Serializable]
  public class grupoOpcional : IExtensibleDataObject, INotifyPropertyChanged
  {
    [NonSerialized]
    private ExtensionDataObject extensionDataField;
    [OptionalField]
    private string codDisciplinaOpField;
    [OptionalField]
    private string disciplinaOpField;

    [Browsable(false)]
    public ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [DataMember]
    public string codDisciplinaOp
    {
      get
      {
        return this.codDisciplinaOpField;
      }
      set
      {
        if ((object) this.codDisciplinaOpField == (object) value)
          return;
        this.codDisciplinaOpField = value;
        this.RaisePropertyChanged("codDisciplinaOp");
      }
    }

    [DataMember]
    public string disciplinaOp
    {
      get
      {
        return this.disciplinaOpField;
      }
      set
      {
        if ((object) this.disciplinaOpField == (object) value)
          return;
        this.disciplinaOpField = value;
        this.RaisePropertyChanged("disciplinaOp");
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
