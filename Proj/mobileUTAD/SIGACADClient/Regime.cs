﻿// Decompiled with JetBrains decompiler
// Type: mobileUTAD.SIGACADClient.Regime
// Assembly: mobileUTAD, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C6335312-308C-477B-9BDC-E68404688049
// Assembly location: A:\Git repositories\mobileUTAD\bin\mobileUTAD.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace mobileUTAD.SIGACADClient
{
  [DebuggerStepThrough]
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DataContract(Name = "Regime", Namespace = "http://schemas.datacontract.org/2004/07/wsSIGACAD2")]
  [Serializable]
  public class Regime : IExtensibleDataObject, INotifyPropertyChanged
  {
    [NonSerialized]
    private ExtensionDataObject extensionDataField;
    [OptionalField]
    private int AnoLectivoField;
    [OptionalField]
    private string CodCursoField;
    [OptionalField]
    private string CodRegimeField;
    [OptionalField]
    private DateTime DataInicioField;
    [OptionalField]
    private string NomeCursoField;
    [OptionalField]
    private string NomeRegimeField;
    [OptionalField]
    private int NumeroField;

    [Browsable(false)]
    public ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [DataMember]
    public int AnoLectivo
    {
      get
      {
        return this.AnoLectivoField;
      }
      set
      {
        if (this.AnoLectivoField.Equals(value))
          return;
        this.AnoLectivoField = value;
        this.RaisePropertyChanged("AnoLectivo");
      }
    }

    [DataMember]
    public string CodCurso
    {
      get
      {
        return this.CodCursoField;
      }
      set
      {
        if ((object) this.CodCursoField == (object) value)
          return;
        this.CodCursoField = value;
        this.RaisePropertyChanged("CodCurso");
      }
    }

    [DataMember]
    public string CodRegime
    {
      get
      {
        return this.CodRegimeField;
      }
      set
      {
        if ((object) this.CodRegimeField == (object) value)
          return;
        this.CodRegimeField = value;
        this.RaisePropertyChanged("CodRegime");
      }
    }

    [DataMember]
    public DateTime DataInicio
    {
      get
      {
        return this.DataInicioField;
      }
      set
      {
        if (this.DataInicioField.Equals(value))
          return;
        this.DataInicioField = value;
        this.RaisePropertyChanged("DataInicio");
      }
    }

    [DataMember]
    public string NomeCurso
    {
      get
      {
        return this.NomeCursoField;
      }
      set
      {
        if ((object) this.NomeCursoField == (object) value)
          return;
        this.NomeCursoField = value;
        this.RaisePropertyChanged("NomeCurso");
      }
    }

    [DataMember]
    public string NomeRegime
    {
      get
      {
        return this.NomeRegimeField;
      }
      set
      {
        if ((object) this.NomeRegimeField == (object) value)
          return;
        this.NomeRegimeField = value;
        this.RaisePropertyChanged("NomeRegime");
      }
    }

    [DataMember]
    public int Numero
    {
      get
      {
        return this.NumeroField;
      }
      set
      {
        if (this.NumeroField.Equals(value))
          return;
        this.NumeroField = value;
        this.RaisePropertyChanged("Numero");
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
