﻿// Decompiled with JetBrains decompiler
// Type: mobileUTAD.SIGACADClient.ISIGACADChannel
// Assembly: mobileUTAD, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C6335312-308C-477B-9BDC-E68404688049
// Assembly location: A:\Git repositories\mobileUTAD\bin\mobileUTAD.dll

using System;
using System.CodeDom.Compiler;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace mobileUTAD.SIGACADClient
{
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  public interface ISIGACADChannel : ISIGACAD, IClientChannel, IContextChannel, IChannel, ICommunicationObject, IExtensibleObject<IContextChannel>, IDisposable
  {
  }
}
