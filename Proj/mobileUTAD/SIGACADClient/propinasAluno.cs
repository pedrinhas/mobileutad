﻿// Decompiled with JetBrains decompiler
// Type: mobileUTAD.SIGACADClient.propinasAluno
// Assembly: mobileUTAD, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C6335312-308C-477B-9BDC-E68404688049
// Assembly location: A:\Git repositories\mobileUTAD\bin\mobileUTAD.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace mobileUTAD.SIGACADClient
{
  [DebuggerStepThrough]
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DataContract(Name = "propinasAluno", Namespace = "http://schemas.datacontract.org/2004/07/wsSIGACAD2")]
  [Serializable]
  public class propinasAluno : IExtensibleDataObject, INotifyPropertyChanged
  {
    [NonSerialized]
    private ExtensionDataObject extensionDataField;
    [OptionalField]
    private string anoLectivoField;
    [OptionalField]
    private string codCursoField;
    [OptionalField]
    private string cursoField;
    [OptionalField]
    private string dataanulacaoField;
    [OptionalField]
    private string datadevolucaoField;
    [OptionalField]
    private string datalimitepagField;
    [OptionalField]
    private string datapagamentoField;
    [OptionalField]
    private string descrField;
    [OptionalField]
    private string entidadeField;
    [OptionalField]
    private string estadoField;
    [OptionalField]
    private string moedaField;
    [OptionalField]
    private string numeroField;
    [OptionalField]
    private string pendenteField;
    [OptionalField]
    private string prestacaoField;
    [OptionalField]
    private string referenciaField;
    [OptionalField]
    private string valorField;

    [Browsable(false)]
    public ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [DataMember]
    public string anoLectivo
    {
      get
      {
        return this.anoLectivoField;
      }
      set
      {
        if ((object) this.anoLectivoField == (object) value)
          return;
        this.anoLectivoField = value;
        this.RaisePropertyChanged("anoLectivo");
      }
    }

    [DataMember]
    public string codCurso
    {
      get
      {
        return this.codCursoField;
      }
      set
      {
        if ((object) this.codCursoField == (object) value)
          return;
        this.codCursoField = value;
        this.RaisePropertyChanged("codCurso");
      }
    }

    [DataMember]
    public string curso
    {
      get
      {
        return this.cursoField;
      }
      set
      {
        if ((object) this.cursoField == (object) value)
          return;
        this.cursoField = value;
        this.RaisePropertyChanged("curso");
      }
    }

    [DataMember]
    public string dataanulacao
    {
      get
      {
        return this.dataanulacaoField;
      }
      set
      {
        if ((object) this.dataanulacaoField == (object) value)
          return;
        this.dataanulacaoField = value;
        this.RaisePropertyChanged("dataanulacao");
      }
    }

    [DataMember]
    public string datadevolucao
    {
      get
      {
        return this.datadevolucaoField;
      }
      set
      {
        if ((object) this.datadevolucaoField == (object) value)
          return;
        this.datadevolucaoField = value;
        this.RaisePropertyChanged("datadevolucao");
      }
    }

    [DataMember]
    public string datalimitepag
    {
      get
      {
        return this.datalimitepagField;
      }
      set
      {
        if ((object) this.datalimitepagField == (object) value)
          return;
        this.datalimitepagField = value;
        this.RaisePropertyChanged("datalimitepag");
      }
    }

    [DataMember]
    public string datapagamento
    {
      get
      {
        return this.datapagamentoField;
      }
      set
      {
        if ((object) this.datapagamentoField == (object) value)
          return;
        this.datapagamentoField = value;
        this.RaisePropertyChanged("datapagamento");
      }
    }

    [DataMember]
    public string descr
    {
      get
      {
        return this.descrField;
      }
      set
      {
        if ((object) this.descrField == (object) value)
          return;
        this.descrField = value;
        this.RaisePropertyChanged("descr");
      }
    }

    [DataMember]
    public string entidade
    {
      get
      {
        return this.entidadeField;
      }
      set
      {
        if ((object) this.entidadeField == (object) value)
          return;
        this.entidadeField = value;
        this.RaisePropertyChanged("entidade");
      }
    }

    [DataMember]
    public string estado
    {
      get
      {
        return this.estadoField;
      }
      set
      {
        if ((object) this.estadoField == (object) value)
          return;
        this.estadoField = value;
        this.RaisePropertyChanged("estado");
      }
    }

    [DataMember]
    public string moeda
    {
      get
      {
        return this.moedaField;
      }
      set
      {
        if ((object) this.moedaField == (object) value)
          return;
        this.moedaField = value;
        this.RaisePropertyChanged("moeda");
      }
    }

    [DataMember]
    public string numero
    {
      get
      {
        return this.numeroField;
      }
      set
      {
        if ((object) this.numeroField == (object) value)
          return;
        this.numeroField = value;
        this.RaisePropertyChanged("numero");
      }
    }

    [DataMember]
    public string pendente
    {
      get
      {
        return this.pendenteField;
      }
      set
      {
        if ((object) this.pendenteField == (object) value)
          return;
        this.pendenteField = value;
        this.RaisePropertyChanged("pendente");
      }
    }

    [DataMember]
    public string prestacao
    {
      get
      {
        return this.prestacaoField;
      }
      set
      {
        if ((object) this.prestacaoField == (object) value)
          return;
        this.prestacaoField = value;
        this.RaisePropertyChanged("prestacao");
      }
    }

    [DataMember]
    public string referencia
    {
      get
      {
        return this.referenciaField;
      }
      set
      {
        if ((object) this.referenciaField == (object) value)
          return;
        this.referenciaField = value;
        this.RaisePropertyChanged("referencia");
      }
    }

    [DataMember]
    public string valor
    {
      get
      {
        return this.valorField;
      }
      set
      {
        if ((object) this.valorField == (object) value)
          return;
        this.valorField = value;
        this.RaisePropertyChanged("valor");
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
